<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\{Package,Destination,Order,Checkavailablepackage,Quote};
use App\Http\Controllers\Backend\APIController;
use App\Http\Controllers\Frontend\WebhookController;
use Carbon\Carbon;

class DepositDeadline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposit:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the deposit payment deadline every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(APIController $sendblock,WebhookController $webhook)
    {
        parent::__construct();
        $this->sendblock = $sendblock;
        $this->webhook = $webhook;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $current_time = Carbon::now();
      $order = Order::get();
      foreach ($order as $key => $value) {
          $payment_deadline = $value->payment_deadline;
          if($current_time > $payment_deadline && $value->status == "hold" ){
            $this->sendblock->sendOrderCancelBlock($value->messenger_user_id);
            $this->webhook->orderCancel($value->order_id);
          }
      }
      $this->info('Sent the cancel message to the user');
    }
}
