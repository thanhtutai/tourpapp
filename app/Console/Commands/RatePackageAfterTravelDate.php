<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\{Package,Destination,Order,Checkavailablepackage,Quote};
use Carbon\Carbon;
use App\Http\Controllers\Backend\APIController;

class RatePackageAfterTravelDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:packages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send rating block to users who traveled from chatfuel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(APIController $sendblock)
    {
        parent::__construct();
        $this->sendblock = $sendblock;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $current_time = Carbon::now();
      $order = Order::get();
      foreach ($order as $key => $value) {
          $order_status = $value->status;
          $tour_leader_name = isset($value->package->tour_leader_name)?$value->package->tour_leader_name:"Mr.Aung Aung";
          $package_end_date = isset($value->package->end_date)?Carbon::parse($value->package->end_date)->format('Y-m-d h:m:s'):'2015-12-19 10:10:54';
          if($order_status == 'fully-paid' && $current_time > $package_end_date && $value->sent_rate_package == 0){
            $this->sendblock->sendRatePackageBlock($value->messenger_user_id,$value->package_id,$value->package->name,$value->order_id,$tour_leader_name);
            $order_update = Order::firstOrFail()->where('order_id', $value->order_id)->first();
            $order_update->sent_rate_package = 1;
            $order_update->save();
          }
      }
      $this->info('Sent the rate package block to the user');
    }
}
