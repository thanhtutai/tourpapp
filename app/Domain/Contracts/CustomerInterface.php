<?php

namespace App\Domain\Contracts;

use Illuminate\Http\Request;
/**
 * Interface DestinationInterface
 * @package App\Domain\Contracts
 */
interface CustomerInterface {

    public function getAll(Request $request);

    public function create(Request $request);
    
    public function update(Request $request, $id);
       
}