<?php

namespace App\Domain\Contracts;

use Illuminate\Http\Request;
/**
 * Interface OperatorInterface
 * @package App\Domain\Contracts
 */
interface OperatorInterface {

    public function getAll(Request $request);

    public function create(Request $request);

    public function update(Request $request, $id);   
    
}