<?php

namespace App\Domain\Contracts;

use Illuminate\Http\Request;
/**
 * Interface OrderInterface
 * @package App\Domain\Contracts
 */
interface OrderInterface {

    public function getAll();

    public function orderApprove(Request $request,$id);
    
    public function approveInvoiceImgStore(Request $request,$id);

    public function update(Request $request, $id);

}