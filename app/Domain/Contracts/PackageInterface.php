<?php

namespace App\Domain\Contracts;

use Illuminate\Http\Request;
/**
 * Interface PackageInterface
 * @package App\Domain\Contracts
 */
interface PackageInterface {

    public function getAll(Request $request);

    public function create(Request $request);

    public function getPackage($id);

    public function getPackageInquiry($id);

    public function update(Request $request, $id);

    public function packageInquiryUpdate(Request $request,$id);

}