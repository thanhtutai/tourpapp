<?php

namespace App\Domain\Repositories;

use App\Domain\Contracts\CustomerInterface;
use App\Http\Controllers\Backend\APIController;
use Illuminate\Http\Request;
use App\Models\{Customer};
use Illuminate\Support\Facades\{Input,Session};
use DB;
/**
 * Class CustomerRepository
 * @operator App\Domain\Repositories
 */
class CustomerRepository implements CustomerInterface
{  
    private $customer;

    public function __construct(Customer $customer) {
        $this->customer = $customer;
    }

    public function getAll(Request $request)
    {   
  		if(count($request->all())!=0){  
            $customers = Customer::Search($request)->paginate(10);      
        }
        else{
            $customers = Customer::paginate(12);
        }  

        return $customers;
    }

    public function create(Request $request)
    {    
        $validatedData = $request->validate([
         'descriptions' => 'required',
        ]);

        $destination = new Destination();
      
        $destination_latest_row = Destination::orderBy('id', 'desc')->first();

        if(!empty($destination_latest_row)){
        	$destination->destination_id = $destination_latest_row->id + 1;
        }
        else{
        	$destination->destination_id = 1;
        }   

        $destination->name = Input::get('name');
        $destination->starting_price = Input::get('starting_price');
        $destination->description = Input::get('descriptions');
        $destination->country_category = Input::get('country_category_id');

        if($destination->save()){    
	        if($request->hasFile('image')){
	            $destination->addMediaFromRequest('image')->toMediaCollection('destinationimages');
	        }
	        return $destination;
        }
        else{
            return redirect()->route('destination.index')->with('error','Data is not saved!');
        }
        
    }

   	public function update(Request $request, $id)
    {   
  
        $customer = Customer::find($id);
        $customer->name = Input::get('name');
        $customer->first_name = Input::get('first_name');
        $customer->last_name = Input::get('last_name');
        $customer->ph_num = Input::get('ph_num');
        $customer->email = Input::get('email');

        if($customer->save()){
        	return $customer;
        }
        else{
            return redirect()->route('customer.index')->with('error','Data is not saved!');
        }
    }

}