<?php

namespace App\Domain\Repositories;

use App\Domain\Contracts\DestinationInterface;
use App\Http\Controllers\Backend\APIController;
use Illuminate\Http\Request;
use App\Models\{Operator,Destination,Program};
use Illuminate\Support\Facades\{Input,Session};
use DB;
// use Spatie\MediaLibrary\Models\Media;
/**
 * Class DestinationRepository
 * @operator App\Domain\Repositories
 */
class DestinationRepository implements DestinationInterface
{  
    private $destination;

    public function __construct(Destination $destination) {
        $this->destination = $destination;
    }

    public function getAll(Request $request)
    {   
  		if(count($request->all())!=0){  
            $destinations = Destination::Search($request)->paginate(10);      
        }
        else{
            $destinations = Destination::paginate(12);
        }  

        return $destinations;
    }

    public function create(Request $request)
    {    
        $validatedData = $request->validate([
         // 'descriptions' => 'required',
        ]);

        $destination = new Destination();
      
        $destination_latest_row = Destination::orderBy('id', 'desc')->first();

        if(!empty($destination_latest_row)){
        	$d_id = $destination_latest_row->id + 1;
        }
        else{
        	$d_id = 1;
        }   

        $data = [
           'destination_id' => $d_id,
          'en' => ['name' => Input::get('name')],
          'zg' => ['name' => Input::get('zg_name')],
        ];
        $destination = Destination::create($data);


        // $destination->translate('en')->name = Input::get('name');
        // $destination->translate('zg')->name = Input::get('zg_name');
        return $destination;

        // if($destination->save()){    
	       //  if($request->hasFile('image')){
	       //      $destination->addMediaFromRequest('image')->toMediaCollection('destinationimages');
	       //  }
	       //  return $destination;
        // }
        // else{
        //     return redirect()->route('destination.index')->with('error','Data is not saved!');
        // }
        
    }

   	public function update(Request $request, $id)
    {
        
        $destination = Destination::find($id);
        // dd($destination->translate('en')->name);
        $destination->translate('en')->name = Input::get('name');
        $destination->translate('zg')->name = Input::get('zg_name');
       
        if($request->hasFile('image')){
             $destination->media()->delete($id);
            // Media::whereIn('id', $id)->delete();
        	// $destination->clearMediaCollection('destinationimages');
        	$destination->addMediaFromRequest('image')->toMediaCollection('destinationimages');
        }

        if($destination->save()){
        	return $destination;
        }
        else{
            return redirect()->route('destination.index')->with('error','Data is not saved!');
        }
    }

}