<?php

namespace App\Domain\Repositories;

use App\Domain\Contracts\OperatorInterface;
use App\Http\Controllers\Backend\APIController;
use Illuminate\Http\Request;
use App\Models\{Operator,Destination,Program};
use Illuminate\Support\Facades\{Input,Session};
use DB;
/**
 * Class OperatorRepository
 * @operator App\Domain\Repositories
 */
class OperatorRepository implements OperatorInterface
{
    private $operator;

    public function __construct(Operator $operator) {
        $this->operator = $operator;
    }

    public function getAll(Request $request)
    {   
        if(count($request->all())!=0){  
            $operators = Operator::Search($request)->orderBy('id','desc')->paginate(10);   
        }
        else{
            $operators = Operator::orderBy('id','desc')->paginate(12);
        } 

        return $operators;
    }

    public function create(Request $request)
    {    
        $operator = new Operator();
        $operator_latest_row = Operator::orderBy('id', 'desc')->first();
        $operator->operator_id= $operator_latest_row->id + 1;
        $operator->name = Input::get('name');
        $operator->address = Input::get('address');
        $operator->ph_num = Input::get('ph_num');
        $operator->email = Input::get('email');
        $operator->user_id = Input::get('user_id');
        $operator->rating = Input::get('rating');
        $operator->review = Input::get('review');
        $operator->messenger_url = Input::get('messenger_url');
        $operator->review_link = Input::get('review_link');

        if($operator->save()){
            return $operator;
        }
        else{
            return redirect()->route('operator.index')->with('error','Data is not saved!');
        }
    }

    public function update(Request $request, $id)
    {
        $operator = Operator::find($id);
        $operator->name = Input::get('name');
        $operator->address = Input::get('address');
        $operator->email = Input::get('email');
        $operator->ph_num = Input::get('ph_num');
        $operator->user_id = Input::get('user_id');
        $operator->rating = Input::get('rating');
        $operator->review = Input::get('review');
        $operator->messenger_url = Input::get('messenger_url');
        $operator->review_link = Input::get('review_link');
        if($operator->save()){
            return $operator;
        }
        else{
            return redirect('operator')->with('error','Data is not saved!');
        }
    }

}