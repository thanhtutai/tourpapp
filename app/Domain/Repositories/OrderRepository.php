<?php

namespace App\Domain\Repositories;

use App\Domain\Contracts\OrderInterface;
use Illuminate\Http\Request;
use App\Models\{Order,Customer};
use Illuminate\Support\Facades\{Input,Session};
/**
 * Class OrderRepository
 * @operator App\Domain\Repositories
 */
class OrderRepository implements OrderInterface
{
    private $order;

    public function __construct(Order $order) {
        $this->order = $order;
    }

    public function getAll()
    {   
        $orders = Order::orderBy('id','desc')->paginate(12);

        return $orders;
    }

    public function orderApprove(Request $request,$id)
    {
        $order = Order::find($id);

        if($request->approval){
            $order->approval = Input::get('approval');
        }
        else{
            $order->approval = 0;
        }

        if($order->save()){
            return $order;
        }
        else{
            return redirect()->route('order.index')->with('error','Data is not saved!');
        }
    }    
    
    public function approveInvoiceImgStore(Request $request,$id)
    {

        $order = Order::find($id);

        $order->addMediaFromRequest('image')->toMediaCollection('approve_invoice_img');

        return $order;
       
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $customer = Customer::firstOrFail()->where('messenger_user_id',$order->messenger_user_id)->first();
        // dd($request);
        // dd($customer);
        $customer->name = Input::get('name');
        $customer->ph_num = Input::get('ph_num');
        $order->quantity = Input::get('quantity');
        $order->price_each = Input::get('price_each');
        $customer->email = Input::get('email');
        $order->status = Input::get('status');

        if($request->approval){
            $order->approval = Input::get('approval');
        }
        else{
            $order->approval = 0;
        }

        if($order->save() && $customer->save()){

            return back()->with('success','Order was successfully Approved!');
        }
        else{
            return redirect('order');
        }
    }



}