<?php

namespace App\Domain\Repositories;

use App\Domain\Contracts\PackageInterface;
use App\Http\Controllers\Backend\APIController;
use Illuminate\Http\Request;
use App\Models\{Package,Destination,Program,InquiryPackage};
use Illuminate\Support\Facades\{Input,Session};
use DB;
/**
 * Class PackageRepository
 * @package App\Domain\Repositories
 */
class PackageRepository implements PackageInterface
{
    private $package;

    public function __construct(Package $package,InquiryPackage $inquiryPackage,APIController $sendpackageAvailable) {
        $this->package = $package;
        $this->inquiryPackage = $inquiryPackage;
        $this->sendpackageAvailable = $sendpackageAvailable;  
    }

    public function getAll(Request $request)
    {   
      if(count($request->all())!=0){  
          $packages = Package::Search($request)->orderBy('package_id','desc')->paginate(10);      
      }
      else{
          $packages = Package::orderBy('package_id','desc')->paginate(12);
      }   

      return $packages;
    }

    public function getPackage($id)
    {
      return $this->package->find($id);
    }    
    
    public function getPackageInquiry($id)
    {
      return $this->inquiryPackage->find($id);
    }

	  public function packageInquiryUpdate(Request $request,$id)
    {
      $package = InquiryPackage::firstOrFail()->where('id', $id)->first();

      $package->available   = $request->get('available');
      $package->is_replied  = 1;
      $package->user_id     = \Auth::user()->id;

      if($package->save()){
        $avaialble_package = Package::where('package_id',$package->package_id)->first();
        $total_amount_usd = $package->quantity* $avaialble_package->price;
          //send package aviable status to the user
          if($avaialble_package != null && $package->available == 1){
            $this->sendpackageAvailable->sendavailablepackageblock($package->messenger_user_id,$package->package_id,$total_amount_usd);
          }
          else{
            $this->sendpackageAvailable->sendunavailablepackageblock($package->messenger_user_id,$package->package_id);
          }

          return $package;

      }
      else{
          return redirect()->route('package.index');
      }

    }
    
    public function update(Request $request, $id)
    {
      $package = Package::where('package_id',$id)->first();
      $package->name = Input::get('name');
      $package->price = Input::get('price');
      $package->description = Input::get('descriptions');
      // $package->review_rate = Input::get('review_rate');
      $package->start_date = Input::get('start_date');
      $package->end_date = Input::get('end_date');
      $package->operator_id = Input::get('operator_id');
      $package->tour_leader_name = Input::get('tour_leader_name');
      $package->currency_id = Input::get('currency_id');
      // $package->quantity = Input::get('quantity');

      if($request->quantity){
        $package->quantity = 1000;  
      }
      else{
        $package->quantity = 1000;
      }        
      if($request->inquiry){
        $package->inquiry = Input::get('inquiry');  
      }
      else{
        $package->inquiry = 0;
      }
      
      if($request->hasFile('image')) {
        $package->clearMediaCollection('packageimage');
        $package->addMediaFromRequest('image')->toMediaCollection('packageimage');
      }

      $destination_list = DB::table('destination_package')->where('package_id',$id)->get();
  
      foreach ($destination_list as $key => $value) {
       //delete first
        $package->destinations()->detach($value);
      
      }

      if($package->save()){
  
        $destinations = Input::get('destination');
        $package->destinations()->attach($destinations);
        
        $program_list = Program::where('package_id',$id);
        $program_list->delete();

      //for multiple program 
        $data = $request->except('_token');

        // if(!empty($data['program_name'])){
        //   $subject_count = count($data['program_name']);
        
        // // dd($subject_count);
        //   for($i=0; $i < $subject_count; $i++){

        //     $program = new Program;
        //     $program->name = $data['program_name'][$i];
        //     $program_latest_row = Program::orderBy('id', 'desc')->first();

        //     if(!empty($program_latest_row)){
        //       $program->program_id = $program_latest_row->id + 1;
        //     }
        //     else{
        //       $program->package_id = 1;
        //     }  

        //     $package_latest_row_2 = Package::orderBy('id', 'desc')->first();
        //     $program->package_id = $package_latest_row_2->id;
           
        //     $program->save();

        //     }
        // }

        return $package;
      }
      else
      {
        return redirect()->route('package.index');
      }
    }

    public function create(Request $request){
		// dd($request);
      $package = new Package();
	    $package_latest_row = Package::orderBy('id', 'desc')->first();
	    
	    if(!empty($package_latest_row)){
	      $package->package_id = $package_latest_row->id + 1;
	    }
      else{
	      $package->package_id = 1;
	    }   

	    $package->name = Input::get('name');
	    $package->inquiry = Input::get('inquiry');
	    $package->quantity = 1000;
	    $package->price = Input::get('price');
	    $package->description = Input::get('descriptions');
      // $package->review_rate = Input::get('review_rate');
      $package->operator_id = Input::get('operator_id');
	    $package->tour_leader_name = Input::get('tour_leader_name');
	    $package->currency_id = Input::get('currency_id');
	    $package->start_date = Input::get('start_date');
	    $package->end_date = Input::get('end_date');
	    
	    if($package->save())
	    {
        $destinations = new Destination();
  	    $destinations = Input::get('destination');
  	    $package->destinations()->attach($destinations);
  	    
        if($request->hasFile('image')) {
          $package->addMediaFromRequest('image')->toMediaCollection('packageimage');
  	    }

  	    if($request->hasFile('package_detail_img')) {
          $fileAdders = $package
  	        ->addMultipleMediaFromRequest(['package_detail_img'])
  	        ->each(function ($fileAdder) {
  	            $fileAdder->toMediaCollection('package_detail_img');
  	        }); 
  	    }

        $data = $request->except('_token');

        // if(implode(null,$request->program_name)!=null){
        //   $subject_count = count($data['program_name']);
        // }
        // else{
        //   //$arr has some value rather than null
        //   $subject_count = 0;
        // }
      
        // for($i=0; $i < $subject_count; $i++){
        //   $program = new Program;
        //   $program->name = $data['program_name'][$i];
        //   $program->description = $data['program_description'][$i];
        //   // $program->start_date = $data['program_start_date'][$i];
        //   $program_latest_row = Program::orderBy('id', 'desc')->first();
        //   if(!empty($program_latest_row)){
        //     $program->program_id = $program_latest_row->id + 1;
        //   }
        //   else{
        //     $program->package_id = 1;
        //   }   

        //   $package_latest_row_2 = Package::orderBy('id', 'desc')->first();
        //   $program->package_id = $package_latest_row_2->id;
         
        //   $program->save();
        // }
        return $package;
      }
      else
      {
        return redirect()->route('package.index')->with('error','Data is not saved!');
      }
    }

    public function destroy($id)
    {
      return $this->package->destroy($id);
    }


}
