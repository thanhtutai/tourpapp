<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client; 

class APIController extends Controller
{
  public function sendWelcomeChatBlock($messenger_user_id,$is_welcome)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5b7c52fa0ecd9f7e970399c0';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&is_welcome='.$is_welcome;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }      
  public function sendEmptyPackage($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5d305a48f4bfde00010cc1ba';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }    
  public function sendRequestLiveChatBlock($messenger_user_id,$is_live_chat)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5ccd4698cd9d1800066f167b';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&is_live_chat='.$is_live_chat;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }  
  public function sendThankyouRatigInfo($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5cb96a134b7d020006eee7e6';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }  
  public function sendRatePackageBlock($messenger_user_id,$package_id,$package_name,$order_id,$tour_leader_name)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c69546f76ccbc29ae3c72fb';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&package_id='.$package_id.'&package_name='.$package_name.'&order_id='.$order_id.'&tour_leader_name='.$tour_leader_name;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }  
  public function sendDepositWaitingMessage($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c5a621176ccbc6c2a75036e';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }     
  public function sendBookingSaveBlock($messenger_user_id,$booking_code,$order_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c59632a0ecd9f5655b29481';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&booking_code='.$booking_code.'&order_id='.$order_id;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }     
  public function sendapproveInvoiceImgBlock($messenger_user_id,$order_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c5957890ecd9f5655a70ba5';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&order_id='.$order_id;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }    
  public function sendOrderCancelBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c5867a90ecd9f5655d71eab';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }    

  public function sendCheckAvailableFailBlock($messenger_user_id,$package_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c3cb9f476ccbc01c2a11e4d';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&package_id='.$package_id;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }     
  public function sendOrderInquiryFail($messenger_user_id,$package_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c3cb0dc76ccbc01c2951af8';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&package_id='.$package_id;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }   
  public function sendWaitingMessage($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c31d85a0ecd9f2f91ff5702';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }    
  public function sendInfoPaymentBlock($messenger_user_id,$bank_info,$deposit_amt,$exchange_rate,$payment_deadline,$quantity)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c3b42eb76ccbc01c2a8ab79';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&bank_info='.$bank_info.'&deposit_amt='.$deposit_amt.'&exchange_rate='.$exchange_rate.'&payment_deadline='.$payment_deadline.'&quantity='.$quantity;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }  
  public function sendGetQuotation($messenger_user_id,$available_quantity,$start_date,$package_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5c28865276ccbc17baef1d3a';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&available_quantity='.$available_quantity.'&start_date='.$start_date.'&package_id='.$package_id;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }      
  public function sendOrderBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5b8d629776ccbc4eb9e67061';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }    

  public function sendUnavailableOrderBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5bd1a5b376ccbc7651a95735';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }   
  public function sendOrderSuccessBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5babc49d76ccbc4b7f35f0a4';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }     
  public function sendNotAvailableDestinationBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5bc22e9176ccbc480a8ff2bd';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }   
  public function sendNotAvailablePackageBlock($messenger_user_id)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5bc231cd76ccbc480a90a2d3';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
  }     
  public function sendUploadImageBlock($messenger_user_id)
  {
      $bot_id = '5b7c52fa0ecd9f7e970399b1';
      $user_id = $messenger_user_id;
      $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
      $block = '5b9d51920ecd9f263bd22afa';
      $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block;

      $client = new Client;   
          try {
              $response = $client->post($url);
              return json_decode( $response->getBody()->getContents(),true );
          } catch (ClientException $e) {
            return json_decode( $e->getResponse()->getBody()->getContents(),true );
          }  
  }     

  public function sendavailablepackageblock($messenger_user_id,$id,$total_amount_usd)
  {
    $bot_id = '5b7c52fa0ecd9f7e970399b1';
    $user_id = $messenger_user_id;
    $token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
    $block = '5b9549420ecd9f645bb4d396';
    $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&package_id='.$id.'&total_amount_usd='.$total_amount_usd;

    $client = new Client;   
        try {
            $response = $client->post($url);
            return json_decode( $response->getBody()->getContents(),true );
        } catch (ClientException $e) {
          return json_decode( $e->getResponse()->getBody()->getContents(),true );
        }  
    }  
    public function sendunavailablepackageblock($messenger_user_id,$id)
    {
     	$bot_id = '5b7c52fa0ecd9f7e970399b1';
     	$user_id = $messenger_user_id;
     	$token = 'mELtlMAHYqR0BvgEiMq8zVek3uYUK3OJMbtyrdNPTrQB9ndV0fM7lWTFZbM4MZvD';
     	$block = '5b9d3a750ecd9f263bcc819e';
      $url = 'https://api.chatfuel.com/bots/'.$bot_id.'/users/'.$user_id.'/send?chatfuel_token='.$token.'&chatfuel_message_tag=APPLICATION_UPDATE&chatfuel_block_id='.$block.'&package_id='.$id;

     	$client = new Client;   
          try {
              $response = $client->post($url);
              return json_decode( $response->getBody()->getContents(),true );
          } catch (ClientException $e) {
            return json_decode( $e->getResponse()->getBody()->getContents(),true );
          }  
    }
}
