<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
       $currency = Currency::paginate(10);
       return view('backend.currency.index',compact('currency')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // return view("backend.currency.index");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $currency = new Currency();
        // $currency->denomination = Input::get('denomination');


        // if($currency->save())
        // {
        //     Session::flash('message','Currency was successfully created');
        //     Session::flash('m-class','alert-success');
        //     return back()->with('success','Currency successfully added!');
        // }
        // else
        // {
        //     Session::flash('message','Data is not saved');
        //     Session::flash('m-class','alert-danger');
        //     return redirect('backend');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function show(Township $township)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // $currency = Currency::findOrFail($id);
       
        // return view("backend.currency.edit", compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $currency = Currency::find($id);

        // $currency->denomination = Input::get('denomination');

        // if($currency->save())
        // {
        //     Session::flash('message','Currency was successfully updated');
        //     Session::flash('m-class','alert-success');
        //     return redirect('backend/currency')->with('success','Currency successfully updated!');
        // }
        // else
        // {
        //     Session::flash('message','Data is not saved');
        //     Session::flash('m-class','alert-danger');
        //     return redirect('backend');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Township  $township
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
    //     $currency->delete();
    //     return redirect()->route('currency.index')
    //                     ->with('success','Currency deleted successfully');
    
    }
}
