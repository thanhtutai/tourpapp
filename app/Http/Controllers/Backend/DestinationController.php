<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Destination,Destinationimage};
use Illuminate\Support\Facades\{Input,Session};
use Carbon\Carbon;
use Spatie\MediaLibrary\Models\Media;
use App\Domain\Contracts\DestinationInterface;

class DestinationController extends Controller
{

    private $destinationRepository;

    public function __construct(DestinationInterface $destinationRepository)
    {
        $this->destinationRepository = $destinationRepository; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $destinations = $this->destinationRepository->getAll($request);

        return view("backend.destination.index",compact('destinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.destination.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operator = $this->destinationRepository->create($request);

        return back()->with('success','Destination successfully added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destination = Destination::findOrFail($id);
       
        return view("backend.destination.edit", compact('destination'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->destinationRepository->update($request,$id);
        
        return redirect()->route('destination.index')->with('success','Destination successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destination $destination)
    {
        $destination->delete();
        return redirect()->route('destination.index')
                        ->with('success','Destination deleted successfully');
    }
}
