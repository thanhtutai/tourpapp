<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail,Session;
use App\Models\Package;
class EmailController extends Controller
{


 public function sendingEmail(Request $request)
    {   
    	$package = Package::where('package_id',6)->first();
   		$package_info = ['subject'=> 'New inquiry for '.$package->name,'email'=>'thanhtutoo95@gmail.com','package_name'=>  $package->name,'quantity'=>1,'start_date'=>$package->start_date,'operator_name'=>$package->operator->name,'order_id'=>12];

        Mail::send('backend.email.admin_inquiry', ['package_info' => $package_info,'package'=>$package], function ($message) use ($package_info,$package)
        {                 
            $message->to($package_info['email'], $package_info['subject'])->subject('New inquiry for '.$package_info['package_name']);
        }); 
        return 'success';
    } 

}
