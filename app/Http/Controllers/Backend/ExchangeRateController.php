<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExchangeRate;
use Illuminate\Support\Facades\{Input,Session};

class ExchangeRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ex_rates = ExchangeRate::get();
      return view('backend.exchange_rate.index',compact('ex_rates')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ex_rate = new ExchangeRate();
        $ex_rate->bank_info = Input::get('bank_info');
        $ex_rate->exchange_rate = Input::get('ex_rate');
        $ex_rate->date = Input::get('date');


        if($ex_rate->save())
        {
            Session::flash('message','ExchangeRate was successfully created');
            Session::flash('m-class','alert-success');
            return back()->with('success','ExchangeRate successfully added!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return  back()->with('danger',' Data is not saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ex_rate = ExchangeRate::findOrFail($id);
        return view('backend.exchange_rate.edit',compact('ex_rate')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ex_rate = ExchangeRate::find($id);
        $ex_rate->bank_info = Input::get('bank_info');
        $ex_rate->exchange_rate = Input::get('ex_rate');
        $ex_rate->date = Input::get('date');

        if($ex_rate->save())
        {
            Session::flash('message','ExchangeRate was successfully updated');
            Session::flash('m-class','alert-success');
            return redirect()->route('exchange-rate.index')->with('success','ExchangeRate successfully updated!');
        }
        else
        {
            Session::flash('message','Data is not saved');
            Session::flash('m-class','alert-danger');
            return  back()->with('danger',' Data is not saved!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
