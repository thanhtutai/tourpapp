<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operator;
use Illuminate\Support\Facades\{Input,Session};
use App\User;
use Auth;
use App\Domain\Contracts\OperatorInterface;

class OperatorController extends Controller
{
    private $operatorRepository;

    public function __construct(OperatorInterface $operatorRepository)
    {
        $this->operatorRepository = $operatorRepository; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $operators = $this->operatorRepository->getAll($request);
       
        return view("backend.operator.index",compact('operators')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.operator.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operator = $this->operatorRepository->create($request);

        return back()->with('success','Operator successfully added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operator = Operator::findOrFail($id);
       
        return view("backend.operator.edit", compact('operator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->operatorRepository->update($request,$id);

        return redirect()->route('operator.index')->with('success','Operator successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operator $operator)
    {
        $operator->delete();
        return redirect()->route('operator.index')
                        ->with('success','Operator deleted successfully');
    }
}
