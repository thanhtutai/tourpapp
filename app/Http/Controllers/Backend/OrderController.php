<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;  
use Illuminate\Support\Facades\{Input,Session,Gate,Auth};
use Carbon\Carbon;
use App\Domain\Contracts\OrderInterface;
use App\Http\Controllers\Backend\APIController;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderInterface $orderRepository,APIController $sendblock)
    {
        $this->orderRepository = $orderRepository; 
        $this->sendblock = $sendblock;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->orderRepository->getAll();
       
        return view("backend.order.index",compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('order_id',$id)->first();
        return view('backend.order.show', compact('order','stages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);

        if(Gate::allows('update-order', $order)){
            return view("backend.order.edit", compact('order'));
        }
        else{
            return "you are not allow to edit";
        }
    }    

    public function approveInvoiceImg($id)
    {
        $order = Order::findOrFail($id);

        if(Gate::allows('update-order', $order)){
            return view("backend.order.approve_invoice_img", compact('order'));
        }
        else{
            return "you are not allow to edit";
        }
    }

    public function approveInvoiceImgStore(Request $request,$id)
    {
        // $validatedData = $request->validate([
        //  'image' =>'image|mimes:jpg,png'
        // ]);
        $order = Order::findOrFail($id);

        foreach($order->getMedia('approve_invoice_img') as $my_image){
            // Order::find($id)->deleteMedia($my_image->id);
            $order->media()->delete($id);
        }   
        $this->orderRepository->approveInvoiceImgStore($request,$id);
        
        //send image to the user
        $this->sendblock->sendapproveInvoiceImgBlock($order->messenger_user_id,$order->order_id);

        return back()->with('success','Invoice successfully Sent!');
    }

    public function approveInvoiceImgDestroy(Request $request,$orderid,$fileid)
    {
        $order = Order::findOrFail($orderid);
        $order->getMedia('approve_invoice_img')
              ->keyBy('id')
              ->get($fileid)
              ->delete();
  
        return redirect()->back()->with('status', 'Media file deleted!');

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function orderApprove(Request $request,$id)
    {
        $this->orderRepository->orderApprove($request,$id);
        
        return back()->with('success','Order was successfully Updated!');
    }

    public function update(Request $request, $id)
    {    
        $this->orderRepository->update($request,$id);

        return back()->with('success','Order was successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->route('order.index')->with('success','Order deleted successfully');
    }
}

