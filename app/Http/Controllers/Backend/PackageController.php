<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\{Input,Session};
use App\Http\Controllers\Backend\APIController;
use App\Domain\Contracts\PackageInterface;
use App\Models\{Package,InquiryPackage,Packageimage,Destination,Program};
use Carbon\Carbon;
use DB;

class PackageController extends Controller
{
    protected $sendpackageAvailable;
    private $packageRepository;

    public function __construct(APIController $sendpackageAvailable,PackageInterface $packageRepository)
    {
        $this->sendpackageAvailable = $sendpackageAvailable;  
        $this->packageRepository = $packageRepository; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
       $packages = $this->packageRepository->getAll($request);

       return view("backend.package.index",compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("backend.package.create");
    }

    public function checkPackage(Request $request, $id)
    {
        $package = InquiryPackage::where('id',$id)->first();

        return view('backend.package.check_available',compact('package'));
    }

    public function packageImageUpload(Request $request,$id)
    {
        $package = Package::findOrFail($id);
        $package->addMedia($request->file)->toMediaCollection('package_detail_img');
      
        return redirect()->back()->with('success', 'Media files added to photo album!');
    }

    public function packageImageDestroy(Request $request,$packageid,$fileid)
    {
        $package = Package::findOrFail($packageid);
        $package->getMedia('package_detail_img')
              ->keyBy('id')
              ->get($fileid)
              ->delete();
  
        return redirect()->back()->with('status', 'Media file deleted!');

    }

    public function packageInquiryUpdate(Request $request,$id)
    {
        $this->packageRepository->packageInquiryUpdate($request,$id);
        
        return back()->with('success','Package status was successfully replied in messenger.');
    }

    public function checkPackageList()
    {
        $packages= InquiryPackage::orderBy('id','desc')->paginate(10);

        return view('backend.package.check_available_package_list',compact('packages'));
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $package = $this->packageRepository->create($request);

        return redirect()->route('package.index')->with('success','Package successfully added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::where('package_id',$id)->first();

        return view("backend.package.edit", compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->packageRepository->update($request,$id);
        
        return redirect()->route('package.index')->with('success','Package successfully updated!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return redirect()->route('package.index')
                        ->with('success','Package deleted successfully');
    }
}
