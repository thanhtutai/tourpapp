<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Quote};
class QuoteController extends Controller
{

    public function index(Request $request)
    {   
  		// if(count($request->all())!=0){  
    //         $quotes = Quote::Search($request)->paginate(10);      
    //     }
    //     else{
        $quotes = Quote::paginate(12);
        // }  

        return view("backend.quote.index",compact('quotes'));
    }

}
