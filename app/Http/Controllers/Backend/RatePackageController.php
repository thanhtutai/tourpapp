<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{RatePackage,Order,Package};
use App\Http\Controllers\Backend\APIController;
use App\Jobs\{SendEmailRatePackageJob};
class RatePackageController extends Controller
{

    protected $sendblock;    
    public function __construct(APIController $sendblock)
    {
      $this->sendblock = $sendblock;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        $package = Package::where('package_id',$requestData['package_id'])->first();
        $data_array = [
             'chatfuel_user_id' => $requestData['chatfuel_user_id'],
             'messenger_user_id'=> $requestData['messenger_user_id'],
             'order_id'=> $requestData['order_id'],
             'hotel_during_trip' => $requestData['rate_hotel'],
             'meal' => $requestData['rate_meal'],
             'transportation' => $requestData['rate_transportation'],
             'tour_leader' => $requestData['rate_tour_leader'],
             'tour_guide' => $requestData['rate_tour_guide'],
             'overall_service' => $requestData['rate_overall_service']
                 ];
        $rate_package = RatePackage::Create($data_array);
        if(!empty($rate_package)){

            $order_update = Order::firstOrFail()->where('order_id', $requestData['order_id'])->first();
            $order_update->is_rate_package = 1;
            $order_update->save();       
            $this->sendblock->sendThankyouRatigInfo($requestData['messenger_user_id']);
            SendEmailRatePackageJob::dispatch($rate_package);
            return response()->json(['success' => 'success'], 200);   
       
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
