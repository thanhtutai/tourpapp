<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\APIController;
use App\Models\{Package,Destination,Order,Quote,ExchangeRate};
use Carbon\Carbon;

class DepositController extends Controller
{
    protected $sendblock;
    protected $jwt_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTUzOTM2ODkzMiwibmJmIjoxNTM5MzY4OTMyLCJqdGkiOiJTd0ZjSHQ4bXdVR2ZqYzNyIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.wt43AJuj7lK4TYk9HPSf9zfH1pn_Ff81Rd5cS20oLs0";
    
    public function __construct(APIController $sendblock)
    {
      $this->sendblock = $sendblock;
    }
    public function exchange_rate_info(Request $request)
    {
      $requestData = $request->all();
      $ex_rate = ExchangeRate::first();
      $exchange_rate = $ex_rate->exchange_rate;
      $bank_info = "Bank info -".$ex_rate->bank_info;
      $package = Package::where('package_id',$requestData['package_id'])->first();
      $total_amount_mmk = $requestData['quantity']*$exchange_rate*$package->price;
      return [
             
                 "messages" =>[ [
                            'text' => $bank_info."\n".'Exchange Rate -'.$exchange_rate."\n".'Total Amount in MMK -'.$total_amount_mmk,
                           ]
                          ]
              ]; 

    }
    public function infoPayment(Request $request)
    {

      $requestData = $request->all();

      $package = Package::where('package_id',$requestData['package_id'])->first();
      $ex_rate = ExchangeRate::first();
      $exchange_rate = $ex_rate->exchange_rate;
      $quantity = $requestData['quantity'];
      $deposit_usd_amt = $package->price*0.2*$quantity;
      $deposit_amt = $deposit_usd_amt*$exchange_rate;
      $payment_deadline = Carbon::now()->addDay(1);
      $bank_info = "Bank info -".$ex_rate->bank_info;

      $this->sendblock->sendInfoPaymentBlock($requestData['messenger_user_id'],$bank_info,$deposit_amt,$exchange_rate,$payment_deadline,$quantity);

    }

    public function bookingSaveMsg(Request $request)
    {

   	  $requestData = $request->all();

      $order = Order::where('order_id',$requestData['order_id'])->first();

      $set_attributes = [
                          "booking_code" =>$order->booking_code,
                          "order_id" =>$order->order_id
                        ];
      $upload_slip_btn = [
                          [
                            "set_attributes" => $set_attributes
                            ,
                            "type" =>"show_block",
                            "block_names" =>
                              [
                               'Upload Slip'
                              ],
                            "title" =>"Upload slip"
                          ],
                         ];
      $booking_data[] = [ 
                          "title"=>"please upload bank receipt when you're done.",
                          "image_url"=>"" ,
                          "subtitle"=>'',
                          "buttons"=> $upload_slip_btn
                        ];  

      return ["messages"=>
                     [ 
                      ["attachment"=>
                       [
                        "type"=>"template",
                        "payload"=>
                          [ "template_type"=>"generic",
                            "image_aspect_ratio"=>"square",
                            "elements"=> $booking_data 
                          ]     
                       ]
                      ]
                     ]
             ];

    }

    public function uploadSlip(Request $request)
    {
      $requestData = $request->all();

      if($requestData['payment_type'] == "Deposit" && strpos($requestData['upload_slip'], 'http') === 0 ){
          
          $order_id = $requestData['order_id'];
          $order = Order::find($order_id);
          $order->upload_slip = $requestData['upload_slip'];
          $order->status = "waiting approve";

          $order->save();
          $this->sendblock->sendDepositWaitingMessage($requestData['messenger_user_id']);
      }else{
          
          $this->sendblock->sendUploadImageBlock($requestData['messenger_user_id']);
      }

      return "success";

    }    


}


