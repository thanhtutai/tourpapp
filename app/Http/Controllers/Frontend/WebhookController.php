<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\APIController;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;  
use Mail,Session;
use Carbon\Carbon;
use App;
use App\Models\{Package,Destination,Order,Quote,InquiryPackage,Faq,Customer,Month};
use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Log;
use App\Jobs\{SendHoldEmailJob,SendApproveInvoiceMailJob,SendEmailRequestQuoteJob,SendInquiryMailJob};
class WebhookController extends Controller
{

    protected $sendblock;
    protected $jwt_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTUzOTM2ODkzMiwibmJmIjoxNTM5MzY4OTMyLCJqdGkiOiJTd0ZjSHQ4bXdVR2ZqYzNyIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.wt43AJuj7lK4TYk9HPSf9zfH1pn_Ff81Rd5cS20oLs0";
    
    public function __construct(APIController $sendblock)
    {
      $this->sendblock = $sendblock;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function welcomeText(Request $request)
    {    
      $requestData = $request->all();
      if(!empty($requestData['first_name'])){
        $first_name = $requestData['first_name'];
      }else{
        $first_name ="friend";
      }
            // return [
             
            //      "messages" =>[ [
            //                 'text' => trans('messages.welcome_text', [ 'first_name' => $first_name ]),
            //                ]
            //               ]
            //   ]; quick rep
       $welcome_text[] =  [ "title"=>trans('messages.welcome_text', [ 'first_name' => $first_name ]),
                             "subtitle"=>"",
                             "buttons"=>  [
                                          [
                                            "type"=>"show_block",
                                            "block_names"=>['Choose Destination'],
                                            "title"=>trans('messages.show')
                                          ]
                                        ] 
                               
                            ];  

              return ["messages"=>[ 
                            ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"button",                                
                                  "text"=> trans('messages.welcome_text', [ 'first_name' => $first_name ]), 
                                   "buttons"=>  [
                                          [
                                            "type"=>"show_block",
                                            "block_names"=>['Choose Destination'],
                                            "title"=>trans('messages.show')
                                          ]
                                        ] 
                               
                                       
                                ]     
                              ]
                            ]
                           ]
                          ];


    }    
    public function packageDescription(Request $request)
    {    
      $locale = app()->getLocale();
      $requestData = $request->all();
      // dd($request['package_id']);
      $package = Package::where('package_id',$request['package_id'])->first();
      // return [
       
      //      "messages" =>[ [
      //                 'text' => strip_tags($package->description)
      //                ]
      //               ]
      //   ]; 
      $count_char = strlen(strip_tags($package->description));
      if($count_char > 500){
        $description = str_split(strip_tags($package->description),500);
        foreach ($description as $key => $value) {
          $desc[] = ['text'=>mb_convert_encoding($value, 'UTF-8', 'UTF-8')];
        }
      }else{
        $desc = ['text'=>mb_convert_encoding(strip_tags($package->description),'UTF-8', 'UTF-8')];
      }
      return ["messages"=>[$desc, 
                            ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"button",                                
                                  "text"=> 'Here you go!!', 
                                   "buttons"=>  [
                                          [
                                              "set_attributes" =>
                                              [
                                                "package_id" =>$package->package_id,
                                                "operator_name" =>$package->operator->name,
                                                "operator_id" =>$package->operator->id
                                              ],
                                            "type" =>"show_block",
                                            "block_names" =>[trans('messages.inquiry_question_block')],
                                            "title" =>"Ask Detail"
                                          ],
                                          [
                                          "set_attributes" =>
                                              [
                                                "destination_id" =>$package->destinations[0]->destination_id
                                              ],
                                            "type" =>"phone_number",
                                            "phone_number"=>$package->operator->ph_num?$package->operator->ph_num:'0999',
                                            "title" =>"Call Now"
                                          ],                                 
                                          [
                                          "set_attributes" =>
                                              [
                                                "destination_id" =>$package->destinations[0]->destination_id
                                              ],
                                            "type"=>'json_plugin_url',
                                            "url"=>url('/api/'.$locale.'/package-list/?destination_id='.$package->destinations[0]->destination_id),
                                            "title"=>"Choose Others"
                                          ]
                                        ] 
                               
                                       
                                ]     
                              ]
                            ]
                           ]
                          ];



    }
    public function monthList(Request $request)
    {    
       $locale = app()->getLocale();
        // session()->put('locale', $locale);
        // dd(trans('messages.title') );
       $current_month = date('n');
       
       $month_list = Month::translatedIn($locale)->where('id','>=',$current_month)->get()->toArray();
        // dd($month_list);
       $month_count = count($month_list);
        if(count($month_list) != 0){
            for ($x = 0; $x < $month_count/3; $x++) {

             $chunk = array_chunk($month_list, 3);
             // dd($chunk);
             $button = $chunk[$x];

             foreach ($button as $key => $value) {
                 foreach ($value['translations'] as $key => $arr) {
                    if($arr['locale'] == $locale){
                        $trans = $arr;
                    }
                 }
                 // dd($test);
                 $buttons[] = [
                                    "set_attributes" =>
                                                        [
                                                          "month_id" =>$value['id'],
                                                        ],
                                    "type"=>'show_block',
                                    "block_names"=>[trans('messages.quantity_question_block')],
                                    "title"=>'    '.$trans['name'].'  '
                                ];

             }
        }

        for ($x = 0; $x < $month_count/3; $x++) {
            $chunk_2 = array_chunk($buttons, 3);
            // dd($price_chunk[$x][0]);
            $destination_list_array[]  = 

                [
                   "title"=>trans('messages.month_list').' '.trans('messages.'.(1+$x)),
                   // "image_url"=>url('/media/728/conversions/AyaL-Korea-thumb.jpg') ,
                   // "subtitle"=>'From USD '.$price_chunk[$x][0], 
                   "subtitle"=>'', 
                   "buttons"=> 
                     
                      $chunk_2[$x]
                      
                ];    
        }
          $result = $destination_list_array;
          // dd($result);
          return ["messages"=>[ 
                           ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"generic",
                                  "image_aspect_ratio"=>"square",
                                  "elements"=>$result                                   
                                ]     
                              ]
                           ]
                          ]
              ]; 

      }
    }

    public function quantityQuestion(Request $request)
    {
      $locale = app()->getLocale();

    }
    public function activateLiveChat(Request $request)
    {
      $requestData = $request->all();
      if($requestData['is_live_chat'] == 1)
      {
        $this->sendblock->sendRequestLiveChatBlock($requestData['messenger_user_id'],$requestData['is_live_chat']);
      }
    }


    public function destinationList(Request $request)
    {
        $requestData = $request->all();

        // $destination_list = Destination::whereHas('packages', function($q){
                          // $q->where('start_date', '>', Carbon::now());
                          // $q->whereRaw('quantity > quantity_sold');})->get()->toArray();
        $locale = app()->getLocale();
        // session()->put('locale', $locale);
        // dd(trans('messages.title') );
        $destination_list = Destination::translatedIn($locale)->get()->toArray();
        $destination_count = count($destination_list);
        if(count($destination_list) != 0){
            for ($x = 0; $x < $destination_count/3; $x++) {

             $chunk = array_chunk($destination_list, 3);
             // dd($chunk);
             $button = $chunk[$x];

             foreach ($button as $key => $value) {
                 foreach ($value['translations'] as $key => $arr) {
                    if($arr['locale'] == $locale){
                        $trans = $arr;
                    }
                 }
                 // dd($test);
                 $buttons[] = [
                                  "set_attributes" =>
                                                    [
                                                      "destination_id" =>$value['id'],
                                                    ],
                                    "type"=>'json_plugin_url',
                                    "url"=>url('/api/'.$locale.'/month-list/'),
                                    "title"=>$trans['name']
                                ];
                $starting_price[]=$value['starting_price'];

             }
        }

        for ($x = 0; $x < $destination_count/3; $x++) {
            $chunk_2 = array_chunk($buttons, 3);
            $price_chunk = array_chunk($starting_price, 3);
            // dd($price_chunk[$x][0]);
            $destination_list_array[]  = 

                [
                   "title"=>trans('messages.countries').' '.trans('messages.'.(1+$x)),
                   // "image_url"=>url('/media/728/conversions/AyaL-Korea-thumb.jpg') ,
                   // "subtitle"=>'From USD '.$price_chunk[$x][0], 
                   "subtitle"=>'', 
                   "buttons"=> 
                     
                      $chunk_2[$x]
                      
                ];    
        }
          $result = $destination_list_array;
          // dd($result);
          return ["messages"=>[ 
                           ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"generic",
                                  "image_aspect_ratio"=>"square",
                                  "elements"=>$result                                   
                                ]     
                              ]
                           ]
                          ]
              ]; 

      }else{

        $requestData = $request->all();
        $this->sendblock->sendNotAvailablePackageBlock($requestData['messenger_user_id']);

      }
    }

    function compare_months($a, $b) 
    {
        return strtotime($a) - strtotime($b);
    }

    public function tourInquiry(Request $request)
    {
      $requestData = $request->all();

      // if(is_numeric($requestData['quantity'])){
     // Log::debug('An informational message.');
     // Log::debug($requestData['haha']);
     // Log::debug($requestData['ball']);
     // Log::debug($requestData['haha']);
     // Log::debug($request);
     // $aa = json_encode($requestData);
    // Log::debug($aa->chatfuel_user_id);
     // Log::debug($requestData['chatfuel_user_id']);

        $data_array = [
               'package_id' =>$requestData['package_id'],
               'destination_id' =>$requestData['destination_id'],
               'month_id' =>$requestData['month_id'],
               'chatfuel_user_id' =>$requestData['chatfuel_user_id'],
               'messenger_user_id' =>$requestData['messenger_user_id'],
               'first_name' =>$requestData['first_name'],
               'last_name' =>$requestData['last_name'],
               'ph_num' =>$requestData['ph_num'],
               'email' =>$requestData['email'],
               'quantity' =>$requestData['quantity'],
               'tour_type' =>$requestData['tour_type'],
               'available' =>0,
               'is_replied' =>0,
               ];

         // $this->sendblock->sendWaitingMessage($requestData['messenger_user_id']);
         $inquiry_package = InquiryPackage::Create($data_array);
               
         SendInquiryMailJob::dispatch($inquiry_package);

      return "success";
    }
  

    public function packageList(Request $request)
    {
        // $month_format = Carbon::parse($month)->format('m');
        $requestData = $request->all();
        $locale = app()->getLocale();
        $package_list = Package::leftjoin('destination_package','packages.package_id','=','destination_package.package_id')->where('destination_package.destination_id','=',$requestData['destination_id'])->orderBy('price','asc')->get();
        if(count($package_list)){
          foreach ($package_list as $key => $value) {
           $package = $value;
          // dd($package);
           $start_date = Carbon::parse($package->start_date)->format('d');
           $end_date = Carbon::parse($package->end_date)->format('d F Y');
           $available_quantity_total = $package->quantity-$package->quantity_sold;
          if($available_quantity_total > 0){
            $available_quantity = $available_quantity_total;


           // if(!isset($package_img)){
           $package_img = $package->getFirstOrDefaultMediaUrl('packageimage');
           // } 

           if($available_quantity != "Sold Out" && $package->inquiry == 0){
             
              $destination_list_array[]  =  

              [
                "title" =>$package->name,
                "image_url" =>url($package_img) ,
                "subtitle" => $package->operator->name."\n".'- '.$package->operator->rating.'(stars)'."\n"."price- ".$package->price,
                "buttons" =>
                [
                 [
                  "set_attributes" =>
                  [
                    "package_id" => $package->package_id,
                    "price_each" =>$package->price,
                    "package_name" =>$package->name,
                    "start_date" =>$start_date,
                    "available_quantity" => $available_quantity
                  ],
                    "type" =>"show_block",
                    "block_names" =>['Order w/o inquiry'],
                    "title" =>"Book"
                 ],
                 ["type" => "element_share"]
                ] 
              ];             
            
            }else{
              $review_link = $package->operator->review_link;
              if($review_link == null){
                $review_link = 'www.facebook.com';
              }
              $destination_list_array[]  =  

              [
                "title" =>$package->name,
                "image_url" =>url($package_img) ,
                "subtitle" => $package->operator->name.", ".'- '.$package->operator->rating."\n".'USD '.$package->price,
                "buttons" =>
                [
                   [
                    "type" =>"web_url",
                    "url" =>$review_link,
                    "title" =>"Read Reviews".'('.$package->operator->review.')'
                  ],
                  [
                    "set_attributes" =>
                    [
                      "package_id" =>$package->package_id,
                      "operator_name" =>$package->operator->name,
                      "operator_id" =>$package->operator->id
                    ],
                    "type" =>"show_block",
                    "block_names" =>[trans('messages.inquiry_question_block')],
                    "title" =>"Ask Detail"
                  ],
                  [
                    "set_attributes" =>
                                      [
                                        "package_id" =>$package->package_id,
                                      ],
                    "type"=>'json_plugin_url',
                    "url"=>url('/show-package-img?package_id='.$package->package_id),
                    "title"=>"Tour Summary"
                  ]
                                  
                ] 
              ];   
            }
           }
          }

          $result = $destination_list_array;
          // dd($packages->name);

           return [
             
                 "messages" =>[ 
                           ["attachment" =>
                             ["type" =>"template",
                              "payload" =>
                                [
                                 "template_type" =>"generic",
                                 "image_aspect_ratio" =>"square",
                                 "elements" =>$result                                   
                                ]     
                              ]
                           ]
                          ]
              ]; 

        }else{
          $this->sendblock->sendEmptyPackage($requestData['messenger_user_id']);
        }
    }    
    public function packageList_old($month,$destination_id)
    {
        $month_format = Carbon::parse($month)->format('m');

        $package_list = Package::leftjoin('destination_package','packages.package_id','=','destination_package.package_id')->whereMonth('packages.start_date',$month_format)->where('packages.quantity','!=',0)->where('destination_package.destination_id','=',$destination_id)->orderBy('price','asc')->get();
     
          foreach ($package_list as $key => $value) {
           $package = $value;
          // dd($package);
           $start_date = Carbon::parse($package->start_date)->format('d');
           $end_date = Carbon::parse($package->end_date)->format('d F Y');
           $available_quantity_total = $package->quantity-$package->quantity_sold;
          if($available_quantity_total > 0){
            $available_quantity = $available_quantity_total;

           foreach($package->getMedia('packageimage') as $my_image){
                $package_img = $my_image->getUrl('thumb');
           }   

           if(!isset($package_img)){
              $package_img = $package->getFirstOrDefaultMediaUrl('packageimage');
           } 

           if($available_quantity != "Sold Out" && $package->inquiry == 0){
             
              $destination_list_array[]  =  

              [
                "title" =>$package->name,
                "image_url" =>url($package_img) ,
                "subtitle" => $package->operator->name."\n".$start_date.'-'.$end_date."\n".$package->price.' '.$package->currency->denomination,
                "buttons" =>
                [
                 [
                  "set_attributes" =>
                  [
                    "package_id" => $package->package_id,
                    "price_each" =>$package->price,
                    "package_name" =>$package->name,
                    "start_date" =>$start_date,
                    "available_quantity" => $available_quantity
                  ],
                    "type" =>"show_block",
                    "block_names" =>['Order w/o inquiry'],
                    "title" =>"Book"
                 ],
                 ["type" => "element_share"]
                ] 
              ];             
            
            }else{
              
              $destination_list_array[]  =  

              [
                "title" =>$package->name,
                "image_url" =>url($package_img) ,
                "subtitle" => $package->operator->name."\n".$start_date.'-'.$end_date."\n".$package->price.' '.$package->currency->denomination,
                "buttons" =>
                [
                  
                  [
                    "set_attributes" =>
                    [
                      "package_id" =>$package->package_id
                    ],
                    "type" =>"show_block",
                    "block_names" =>['Check Available'],
                    "title" =>"Check"
                  ],
                  ["type" => "element_share"]
                                  
                ] 
              ];   
            }
           }
          }

          $result = $destination_list_array;
          // dd($packages->name);

           return [
             
                 "messages" =>[ 
                           ["attachment" =>
                             ["type" =>"template",
                              "payload" =>
                                [
                                 "template_type" =>"generic",
                                 "image_aspect_ratio" =>"square",
                                 "elements" =>$result                                   
                                ]     
                              ]
                           ]
                          ]
              ]; 


    }

    public function checkQuantity(Request $request)
    {
      $requestData = $request->all();
      $package = Package::where('package_id',$requestData['package_id'])->first();
      $start_date = Carbon::parse($package->start_date)->format('d F y');

      if(is_numeric($requestData['quantity'])){

        $available_quantity = $package->quantity - $package->quantity_sold;
        
        if($available_quantity >= $requestData['quantity']){
           $this->sendblock->sendOrderBlock($requestData['messenger_user_id']);
        }
        else{
           $this->sendblock->sendGetQuotation($requestData['messenger_user_id'],$available_quantity,$start_date,$package->package_id);
        }
      }
      else{
          $this->sendblock->sendOrderInquiryFail($requestData['messenger_user_id'],$package->package_id);
      }

      return "success";

    }

    public function checkPackage(Request $request)
    {
      $requestData = $request->all();
      $package = Package::where('package_id',$requestData['package_id'])->first();
      
      if(is_numeric($requestData['quantity'])){
     
        $data_array = [
               'package_id' =>$requestData['package_id'],
               'chatfuel_user_id' =>$requestData['chatfuel_user_id'],
               'messenger_user_id' =>$requestData['messenger_user_id'],
               'first_name' =>$requestData['first_name'],
               'last_name' =>$requestData['last_name'],
               'ph_num' =>$requestData['ph_num'],
               'quantity' =>$requestData['quantity'],
               'available' =>0,
               'is_replied' =>0,
               ];

    
        $start_date = Carbon::parse($package->start_date)->format('d F y');
        $available_quantity = $package->quantity - $package->quantity_sold; 
         
        if($requestData['quantity'] > $available_quantity ){
         $this->sendblock->sendGetQuotation($requestData['messenger_user_id'],$available_quantity,$start_date,$package->package_id);
        }
        else{
         $this->sendblock->sendWaitingMessage($requestData['messenger_user_id']);
         $inquiry_package = InquiryPackage::Create($data_array);
               
         SendInquiryMailJob::dispatch($inquiry_package);
  
         // $package_info = ['subject'=> 'New inquiry for '.$package->name,'email'=>'thanhtutoo95@gmail.com','package_name'=>  $package->name,'quantity'=>$requestData['quantity'],'start_date'=>$start_date,'operator_name'=>$package->operator->name,'order_id'=>$inquiry_package->id];

         // $package_info2 = ['subject'=> 'New inquiry for '.$package->name,'email'=>'naingoted+admin@gmail.com','package_name'=>  $package->name,'quantity'=>$requestData['quantity'],'start_date'=>$start_date,'operator_name'=>$package->operator->name,'order_id'=>$inquiry_package->id];

         // Mail::send('backend.email.admin_inquiry', ['package_info' => $package_info,'inquiry_package' => $inquiry_package,'package'=>$package], function ($message) use ($package_info,$package)
         //  {                 
         //      $message->to($package_info['email'], $package_info['subject'])->subject('New inquiry for '.$package_info['package_name']);
         //  });      

         // Mail::send('backend.email.admin_inquiry', ['package_info' => $package_info,'inquiry_package'=>$inquiry_package,'package'=>$package], function ($message) use ($package_info,$package,$package_info2)
         //  {                 
         //      $message->to($package_info2['email'], $package_info['subject'])->subject('New inquiry for '.$package_info['package_name']);
         //  }); 
        }
      }
      else{

         $this->sendblock->sendCheckAvailableFailBlock($requestData['messenger_user_id'],$package->package_id);
      }

      return "success";
    }

    public function requestQuotation(Request $request)
    {

     $requestData = $request->all();

     if($requestData['request_date'] == "Yes"){
      $date = $requestData['start_date'];
     }
     else{
      $date = $requestData['request_date'];
     }

     $data_array = [
               'package_id' =>$requestData['package_id'],
               'chatfuel_user_id' =>$requestData['chatfuel_user_id'],
               'messenger_user_id' =>$requestData['messenger_user_id'],
               'date' =>$date,
               'quantity' =>$requestData['quantity'],
               'ph_num' =>$requestData['ph_num'],
               'first_name' =>$requestData['first_name'],
               'last_name' =>$requestData['last_name']
               ];

     $quote = Quote::Create($data_array); 
     
     SendEmailRequestQuoteJob::dispatch($quote);
     
     return 'success';
    }


    public function availablePackage(Request $request)
    {
        $requestData = $request->all();
        $package = Package::where('package_id',$requestData['package_id'])->first();
        $start_date = Carbon::parse($package->start_date)->format('d F y');
        $start_date_detail = Carbon::parse($package->start_date)->format('d');
        $end_date = Carbon::parse($package->end_date)->format('d F Y');
        foreach($package->getMedia('packageimage') as $my_image){
          $package_img = $my_image->getUrl('thumb');
        }         
        if(!isset($package_img))
        {
          $package_img = $package->getFirstOrDefaultMediaUrl('packageimages');
        } 

        $package_list_array  = 

        [
          "title" =>$package->name,
          "image_url" =>url($package_img) ,
          "subtitle" =>$package->operator->name."\n".$start_date_detail.'-'.$end_date."\n".$package->price.' '.$package->currency->denomination,
          "buttons" =>
            [
             [ 
              "type" =>"show_block",
              "block_names" =>['Order'],
              "title" =>"Book"
             ]                      
            ] 
        ];   

          return [ "set_attributes" =>["package_id" =>$requestData['package_id'],"price_each"=>$package->price,"package_name" =>$package->name,"start_date" =>$start_date
              ],"messages" =>[ 
                           ["attachment" =>
                             ["type" =>"template","payload" =>
                                ["template_type" =>"generic",
                                 "image_aspect_ratio" =>"square",
                                  "elements" =>[$package_list_array]                   
                                ]     
                              ]
                           ]
                          ]
              ];                                 

    }
    public function showPackageImg(Request $request)
    {
     $requestData = $request->all();
     $package = Package::where('package_id',$requestData['package_id'])->first();
   $locale = app()->getLocale();
      foreach($package->getMedia('package_detail_img') as $my_image){
              $package_img = $my_image->getUrl();

              $package_img_list[] = [
                                      "attachment" =>
                                      [
                                        "type" => "image",
                                        "payload" => 
                                        [
                                         "url" => url($package_img)
                                        ]
                                      ]
                                    ];

      }     
      if(!isset($package_img_list)){
              $package_img = $package->getFirstOrDefaultMediaUrl('packageimages');
              $package_img_list = 
                      [
                        ["attachment"=>
                          [  
                           "type" => "image",
                           "payload" => 
                           [
                           "url" => $package_img
                           ]
                          ]
                        ]
                      ];
      }

      $buttons = ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"button",                                
                                  "text"=> 'Interested? Here you go!!', 
                                   "buttons"=>  [
                                          [
                                              "set_attributes" =>
                                              [
                                                "package_id" =>$package->package_id,
                                                "operator_name" =>$package->operator->name,
                                                "operator_id" =>$package->operator->id
                                              ],
                                            "type" =>"show_block",
                                            "block_names" =>[trans('messages.inquiry_question_block')],
                                            "title" =>"Ask Detail"
                                          ],
                                          [
                                          "set_attributes" =>
                                              [
                                                "destination_id" =>$package->destinations[0]->destination_id
                                              ],
                                            "type" =>"phone_number",
                                            "phone_number"=>$package->operator->ph_num?$package->operator->ph_num:'0999',
                                            "title" =>"Call Now"
                                          ],                                 
                                          [
                                          "set_attributes" =>
                                              [
                                                "destination_id" =>$package->destinations[0]->destination_id
                                              ],
                                            "type"=>'json_plugin_url',
                                            "url"=>url('/api/'.$locale.'/package-list/?destination_id='.$package->destinations[0]->destination_id),
                                            "title"=>"Choose Others"
                                          ]
                                        ] 
                               
                                       
                                ]     
                              ]
                            ];
      $array_merge = array_push($package_img_list,$buttons);       
       return ["messages" => $package_img_list,


        ];
     
    }

    public function orderConfirm(Request $request)
    {
        $requestData = $request->all();
        $order = Order::where('package_id',$requestData['package_id'])->first();
        // dd($package_inquiry);
        $package = Package::where('package_id',$order->package_id)->first();

          return [ "messages"=>[ 
                                [ "text" =>"This is your summary -"],
                                [ "text" =>"Name: ".$order->name],
                                [ "text" =>"Package name: ".$package->name],
                                [ "text" =>"Date: ".$package->start_date],
                                [ "text" =>"No of People: ".$order->quantity],
                                [ "text" =>"Phone: ".$order->ph_num],
                                [ "text" =>"Email: ".$order->email, 
                                  "quick_replies"=>[
                                       [
                                         "title"=>"Confirm",
                                         "block_names"=>[
                                            "Order Confirm"
                                         ]
                                       ],[
                                          "title"=>"Cancel",
                                          "block_names"=>[
                                            "Order Cancel"
                                          ]
                                       ]             
                                    ]
                                ]
                               ]
              ];           
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     // Log::info($request);
         $requestData = $request->all();

         if($requestData['payment_type'] == "Make down payment"){

            $this->customerUpdateorCreate($requestData);
            $this->orderCreate($requestData);

            $this->sendblock->sendOrderSuccessBlock($requestData['messenger_user_id']);
         }
         elseif($requestData['payment_type'] == "Deposit"){

            $order_info = $this->orderCreate($requestData);
            $this->customerUpdateorCreate($requestData);
            $order = Order::where("id",$order_info->order_id)->first();
            SendHoldEmailJob::dispatch($order);
            // $order = Order::orderBy("order_id",'desc')->first();
            $this->sendblock->sendBookingSaveBlock($requestData['messenger_user_id'],$order->booking_code,$order->order_id);
    
         }    
         // elseif($requestData['payment_type'] == "Deposit" && strpos($requestData['passport_img'], 'http') === 0 ){

         //    $this->orderCreate($requestData);

         //    $this->sendblock->sendOrderSuccessBlock($requestData['messenger_user_id']);
         // }
         else{
            $this->sendblock->sendUploadImageBlock($requestData['messenger_user_id']);
         }

        return 'success';

    }

    public function customerUpdateorCreate(array $requestData)
    {
      $data_array = [
               'name'             => $requestData['name'],
               'first_name'       => $requestData['first_name'],
               'last_name'        => $requestData['last_name'],
               'chatfuel_user_id' => $requestData['chatfuel_user_id'],
               'messenger_user_id'=> $requestData['messenger_user_id'],
               'ph_num'           => $requestData['ph_num'],
               'email'            => $requestData['email'],
  
          ];
            
          $customer = Customer::updateOrCreate(['messenger_user_id' => $requestData['messenger_user_id']],$data_array);

          return $customer;
    }
    public function orderCreate(array $requestData)
    {

      $order_latest_row = Order::orderBy('id', 'desc')->first();
      $code = "TP";
      $ymd = Carbon::now()->format("ymd");
      $sequence = str_pad($order_latest_row->order_id, 4, '0', STR_PAD_LEFT);
      $booking_code = $code.$ymd.$sequence;
      $order_id = $order_latest_row->order_id + 1;         
      $package = Package::where('package_id',$requestData['package_id'])->first();
      $user_id = $package->operator->user_id;
      if(!empty($requestData['payment_deadline'])){
        // $payment_deadline = Carbon::parse($requestData['payment_deadline'])->format('Y-m-d'); 
        $payment_deadline = $requestData['payment_deadline'];
      }
      $payment_type = $requestData['payment_type'] == "Deposit" ? "Deposit": "Full Payment";
     
      $data_array = [
                 'order_id'         => $order_id,
                 'booking_code'     => $booking_code,
                 'name'             => $requestData['name'],
                 'ph_num'           => $requestData['ph_num'],
                 'email'            => $requestData['email'],
                 'messenger_user_id'=> $requestData['messenger_user_id'],
                 'chatfuel_user_id' => $requestData['chatfuel_user_id'],
                 'price_each'       => $requestData['price_each'],
                 'deposit_amt'      => $requestData['deposit_amt'],
                 'package_id'       => $requestData['package_id'],
                 'quantity'         => $requestData['quantity'],
                 'approval'         => 0,
                 'available'        => 0,
                 'is_replied'       => 0,
                 'status'           => "hold",
                 'payment_type'     => $payment_type,
                 'payment_deadline' => $payment_deadline,
                 'passport_img'     => $requestData['passport_img'],
                 'userd_id'         => $user_id,
                 'is_rate_package'  => 0,
            ];
            
            // $order = Order::updateOrCreate(['messenger_user_id' => $requestData['messenger_user_id']],$data_array);  
            $order = Order::Create($data_array);
            $package = Package::findOrFail($requestData['package_id']);
            $package->quantity_sold = $package->quantity_sold + $requestData['quantity'];
            
            $package->save();

            return $order;
    }

    public function orderCancel($id)
    {
      $order = Order::find($id);
      $order->status   = "canceled";
      $order->save();
      
      return "success";
    }

    public function approveInvoiceImg(Request $request){
     $requestData = $request->all();
     $order = Order::where('order_id',$requestData['order_id'])->first();

      foreach($order->getMedia('approve_invoice_img') as $approve_invoice_image){
              $invoice_img = $approve_invoice_image->getUrl();

              $invoice_img_list[] = ["attachment" =>[
                                                    "type" => "image",
                                                    "payload" => 
                                                    [
                                                     "url" => url($invoice_img)
                                                    ]
                                                  ]
                                    ];

      }     
      if(!isset($invoice_img_list)){
              $invoice_img = $order->getFirstOrDefaultMediaUrl('packageimages');
              $invoice_img_list = 
                      [
                        ["attachment"=>
                          [  
                           "type" => "image",
                           "payload" => 
                           [
                           "url" => $invoice_img
                           ]
                          ]
                        ]
                      ];
      }
      
      $order = Order::find($requestData['order_id']);
      // $order->upload_slip = $requestData['upload_slip'];
      $order->status = "deposited";

      $order->save();
      SendApproveInvoiceMailJob::dispatch($order);

       return ["messages" => $invoice_img_list ];
    }


        public function bookingList(Request $request)
    {
      $requestData = $request->all();

      $order = Order::where('messenger_user_id',$requestData['messenger_user_id'])->orderBy('id','desc')->take(5)->get();
      
      if($order->isEmpty()){
        return [
             
                 "messages" =>[ [
                            'text' => "You don't have any booking yet.",
                           ]
                          ]
              ]; 
      }
      foreach ($order as $key => $value) {
        // $booking_data[] = ["text" => $value->package_id.'-'.$value->name];
        $package_name = isset($value->package->name)?$value->package->name:'Package does not exist';

        $set_attributes = [
                            "booking_code" =>$value->booking_code,
                            "order_id" =>$value->order_id
                          ];
        $upload_slip_btn = [
                              [
                                "set_attributes" => $set_attributes
                                ,
                                "type" =>"show_block",
                                "block_names" =>
                                  [
                                   'Upload Slip'
                                  ],
                                "title" =>"Upload slip"
                              ],
                             ] ;

        $show_slip_btn = [
                            [
                              "set_attributes" =>
                              [
                                "package_id" =>$value->package_id,
                                "order_id" =>$value->order_id
                              ],
                              "type" =>"show_block",
                              "block_names" =>
                                [
                                 'Upload Slip'
                                ],
                              "title" =>"Upload slip"
                            ],
                           ] ;       

        $update_slip_btn = [
                            [
                              "set_attributes" =>
                              [
                                "package_id" =>$value->package_id,
                                "order_id" =>$value->order_id
                              ],
                              "type" =>"show_block",
                              "block_names" =>
                                [
                                 'abc'
                                ],
                              "title" =>"Upload slip"
                            ],
                           ] ;

        
        // return $upload_slip_btn;
        $booking_data[] =  [ "title"=>"Booking Code: ".$value->booking_code,
                             "image_url"=>"" ,
                             "subtitle"=>'Package: '.$package_name,
                             "buttons"=> $upload_slip_btn
                               
                            ];  


      }
      // $booking_data = [["text" => "hah"]];
     
      return ["messages"=>[ 
                            ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"generic",
                                  "image_aspect_ratio"=>"square",
                                  "elements"=> $booking_data 
                                ]     
                              ]
                            ]
                           ]
                          ];

    } 


    public function faqList(Request $request)
    {
      $faqs = Faq::take(9)->get();

      foreach ($faqs as $key => $faq) {
        // $booking_data[] = ["text" => $value->package_id.'-'.$value->name];

        $set_attributes = [
                            "faq_id" =>$faq->id,
                            "faq_page_number" =>"2"
                          ];
        $show_btn = [
                      [
                        "set_attributes" => $set_attributes
                        ,
                        "type" =>"show_block",
                        "block_names" =>
                          [
                           'Faq Answer'
                          ],
                        "title" =>"Show Answer"
                      ],
                     ] ;

      
        // return $upload_slip_btn;
        $faq_data[] =  [ "title"=>$faq->question,
                         "image_url"=>"",
                         "subtitle"=>"",
                         "buttons"=> $show_btn      
                       ];   
      }

      return ["messages"=>[ 
                            ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"generic",
                                  "image_aspect_ratio"=>"square",
                                  "elements"=> $faq_data 
                                ]     
                              ],"quick_replies"=>[
                                       [
                                         "title"=>"more",
                                         "block_names"=>[
                                            "Faq More"
                                         ]
                                       ]            
                                    ]
                            ]
                           ]
                          ];

    }    

    public function faqMore(Request $request)
    {
      $faqs = Faq::skip(9)->take(9)->get();

      foreach ($faqs as $key => $faq) {
        // $booking_data[] = ["text" => $value->package_id.'-'.$value->name];

        $set_attributes = [
                            "faq_id" =>$faq->id,
                            "faq_page_number" =>"2"
                          ];
        $show_btn = [
                      [
                        "set_attributes" => $set_attributes
                        ,
                        "type" =>"show_block",
                        "block_names" =>
                          [
                           'Faq Answer'
                          ],
                        "title" =>"Show Answer"
                      ],
                     ] ;

      
        // return $upload_slip_btn;
        $faq_data[] =  [ "title"=>$faq->question,
                         "image_url"=>"",
                         "subtitle"=>"",
                         "buttons"=> $show_btn      
                       ];   
      }
      // $booking_data = [["text" => "hah"]];

      return ["messages"=>[ 
                            ["attachment"=>
                             [
                              "type"=>"template",
                              "payload"=>
                                [ "template_type"=>"generic",
                                  "image_aspect_ratio"=>"square",
                                  "elements"=> $faq_data 
                                ]     
                              ]
                            ]
                           ]
                          ];

    }
   
    public function faqAnswer(Request $request)
    {
      $requestData = $request->all();

      $faq = Faq::where('id',$requestData['faq_id'])->first();

      
          return [ "messages"=>[ 
                                [ "text" =>$faq->answer]
      
                               ]
              ];           

    }
   

    public function searchPackage(Request $request)
    {   
      $packages = Package::PackageSearch($request)->where('start_date', '>', Carbon::now())->take(5)->get(); 
      // dd($packages); 
      if(count($packages) != 0){
        foreach ($packages as $key => $value) {
          $start_date = Carbon::parse($value['start_date'])->format('d F');
          foreach($value->getMedia('packageimages') as $my_image){
                $package_img = $my_image->getUrl('thumb');
          }                
          //check there is destination img or not.
          if(!isset($package_img)){
            $package_img =  $packages->getFirstOrDefaultMediaUrl('package_images');
          }
          $package_list_array[]  = 

          [
            "title"=> $value['name'],
            "image_url"=>url($package_img),
            "subtitle"=>"Date -".$start_date."\n"."Info -".strip_tags($value['description']),
            "buttons"=>
              [
                [
                  "type"=>"json_plugin_url",
                  "url"=>url('/api/destination/'.$value['destination_id']),
                  "title"=>"Choose"
                ]
              ] 
           ];   
        }
          
        $result = $package_list_array;                                    
        
        return ["messages"=>[ 
                           ["attachment"=>
                             ["type"=>"template","payload"=>
                              ["template_type"=>"generic",
                               "image_aspect_ratio"=>"square",
                               "elements"=>$result                             
                                ]     
                              ]
                           ]
                          ]
              ];    
        
     }else{

       $requestData = $request->all();
       $this->sendblock->sendNotAvailableDestinationBlock($requestData['messenger_user_id']);

     }    
    }



      // public function monthList(Request $request,$id)
    // {
    //     $destination = Destination::where('destination_id',$id)->first();
    //             // $months = array("April", "August", "February");

    //     foreach ($destination->packages as $key => $value) {

    //         $current_date = Carbon::now();
    //         $package_date = $value['start_date'];
    //         $available_quantity_total = $value['quantity']-$value['quantity_sold'];
    //         // dd($available_quantity_total);
    //         if($package_date > $current_date && $available_quantity_total != 0){
    //           $months[] = Carbon::parse($value['start_date'])->format('d-F-Y');
                 
    //         }
    //     }
    //     usort($months, array($this,"compare_months"));
 
    //     foreach ($months as $key => $value) {
    //        $month_format[] =  Carbon::parse($value)->format('F-Y');
    //     }

    //     $package_number_in_month = array_count_values($month_format);
    //     // dd($package_number_in_month);
    //     foreach ($package_number_in_month as $key => $value) {
    //      // foreach($destination->getMedia('destinationimages') as $my_image){
    //      //      $destination_img = $my_image->getUrl('thumb');
    //      //  }   

    //       //check there is destination img or not.
    //       // if(!isset($destination_img)){
    //       //   $destination_img = $destination->getFirstOrDefaultMediaUrl('destinationimages');
    //       // }
    //       // dd($destination_img);
    //      $month_list_array[]  = 
    //     [
    //       "title"=>$key,
    //       "image_url"=>"" ,
    //       "subtitle"=>'We have '.$value.' packages in '.$key,
    //       "buttons"=>
    //       [
    //        [
    //         "type"=>"json_plugin_url",
    //         "url"=>url('/api/package/'.$key.'/'.$destination->destination_id.'?token='.$this->jwt_token),
    //         "title"=>"Choose"
    //         ]
    //       ] 
    //     ];    

    //     } 

    //      return ["messages"=>[ 
    //                    ["attachment" =>
    //                      ["type" =>"template","payload" =>
    //                         [ "template_type" =>"generic",
    //                           "image_aspect_ratio" =>"square",
    //                           "elements" => $month_list_array               
    //                         ]     
    //                       ]
    //                    ]
    //                   ]
    //       ]; 
    // }
}






