<?php

namespace App\Http\Controllers;
 
use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\nRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Jobs\SendEmailJob;
use App\Jobs\SendHoldEmailJob;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Destination;
use Log;
use App;
class HomeController extends Controller
{
    public $loginAfterSignUp = true;
 
    // public function sendEmail()
    // {
    //     // dispatch(new SendEmailJob());
    //     // $emailJob = (new SendEmailJob('Pankaj Sood'))->delay(Carbon::now()->addSeconds(3));
    //     // dispatch($emailJob);
    //     // echo 'email sent';

    //     $order = Order::findOrFail(39);
    //     SendHoldEmailJob::dispatch($order);

    //     Log::info('Dispatched order ' . $order->id);
    //     return 'Dispatched order ' . $order->id;

    // }
    public function lang($locale)
    {
        App::setLocale($locale);
        // dd($locale);
        $zg = Destination::first();
        dd($zg->translate($locale)->name);
        session()->put('locale', $locale);
        dd(trans('messages.title') );
        return view('myHome');
    }
    public function register(RegisterAuthRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
 
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }
 
        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }
 
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;
 
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }
 
        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }
 
    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }
 
    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }
}
