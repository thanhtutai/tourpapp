<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Backend\APIController;
use Closure;

class LiveChat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(APIController $sendblock)
    {
      $this->sendblock = $sendblock;
    }

    public function handle($request, Closure $next)
    {
        $requestData = $request->all();

        $is_live_chat = isset($requestData['is_live_chat'])?$requestData['is_live_chat']:0;
        if($is_live_chat == 1)
        {
            $this->sendblock->sendRequestLiveChatBlock($requestData['messenger_user_id'],$requestData['is_live_chat']);
        }else{
            return $next($request);
        } 
    }
}

