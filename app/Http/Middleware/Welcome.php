<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Backend\APIController;
use Closure;

class Welcome
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(APIController $sendblock)
    {
      $this->sendblock = $sendblock;
    }
    public function handle($request, Closure $next)
    {
        $requestData = $request->all();
        if($requestData['is_welcome'] == 0)
        {
            $this->sendblock->sendWelcomeChatBlock($requestData['messenger_user_id'],$requestData['is_welcome']);
        }else{
            return $next($request);
        } 
    }
}
