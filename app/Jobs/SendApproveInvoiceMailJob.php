<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApproveInvoiceMail;
use Log;
use App\Models\Order;
class SendApproveInvoiceMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $order;
    public function __construct(Order $order)
    {
        $this->order = $order;  
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = isset($this->order->package->operator->email)?$this->order->package->operator->email:'thanhtutoo95@gmail.com';
      Mail::to($email)->send(new ApproveInvoiceMail($this->order));
        // Log::info('Emailed order ' . $this->order->id);
    }
}
