<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Log;
use App\Models\RatePackage;
use App\Mail\SendEmailRatePackage;
class SendEmailRatePackageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $rate_package;
    public function __construct(RatePackage $rate_package)
    {
        $this->rate_package = $rate_package;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $operator_email = isset($this->rate_package->order->package->operator->email)?$this->rate_package->order->package->operator->email:'thanhtutoo95@gmail.com';
      Mail::to($operator_email)->send(new SendEmailRatePackage($this->rate_package));
      Log::info('Emailed RatePackage ' . $this->rate_package->id);
    }
}
