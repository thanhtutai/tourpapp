<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Mail\SendEmailRequestQuote;
use Log;
use App\Models\Quote;
class SendEmailRequestQuoteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $quote;
    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $operator_email = isset($this->quote->package->operator->email)?$this->quote->package->operator->email:'thanhtutoo95@gmail.com';
      Mail::to($operator_email)->send(new SendEmailRequestQuote($this->quote));
      Log::info('Emailed Request Quote ' . $this->quote->id);
    }
}
