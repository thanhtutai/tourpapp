<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\HoldEmail;
use Log;
use App\Models\Order;
class SendHoldEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $order;
    public function __construct(Order $order)
    {
        $this->order = $order;  
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = $this->order->package->operator->email;
      Mail::to($email)->send(new HoldEmail($this->order));
        // Log::info('Emailed order ' . $this->order->id);
    }
}
