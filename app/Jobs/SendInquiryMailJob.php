<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\InquiryMail;
use Log;
use Mail,Session;
//use Illuminate\Support\Facades\Mail;
use App\Models\InquiryPackage;
class SendInquiryMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
        public $inquiry_package;
    public function __construct(InquiryPackage $inquiry_package)
    {
        $this->inquiry_package = $inquiry_package;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $email = isset($this->inquiry_package->package->operator->email)?$this->inquiry_package->package->operator->email:'thanhtutoo95@gmail.com';
      $email_list = [$email,'satan.vvi@gmail.com'];
      Mail::to($email_list)->send(new InquiryMail($this->inquiry_package));
      // Mail::to($email)->send(new InquiryMail($this->inquiry_package));
    }
}

