<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InquiryMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $inquiry_package;
    public function __construct($inquiry_package)
    {
        $this->inquiry_package = $inquiry_package;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'New inquiry for '.$this->inquiry_package->package->name.' package';
        return $this->view('backend.email.admin_inquiry')->replyTo($this->inquiry_package->email,$this->inquiry_package->first_name.' '.$this->inquiry_package->last_name )->subject($subject);  
    }
}
