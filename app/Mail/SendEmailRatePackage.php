<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailRatePackage extends Mailable
{
    use Queueable, SerializesModels;

    public $rate_package;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rate_package)
    {
        $this->rate_package = $rate_package;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'This customer '.$this->rate_package->order->customer->name.' rated this'.$this->rate_package->order->package->name.' for you.';
        return $this->view('backend.email.rate_package')->subject($subject);
    }
}
