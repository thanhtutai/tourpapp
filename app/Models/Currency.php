<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public function operator()
    {
        return $this->belongsTo('App\Models\Operator','operator_id');
    }
}
