<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
  protected $primaryKey = 'messenger_user_id';
  protected $fillable = ['customer_id','name','first_name','last_name','messenger_user_id','chatfuel_user_id','ph_num','email'];
  public function orders()
  {
      return $this->hasMany('App\Models\Order');
  }
}

