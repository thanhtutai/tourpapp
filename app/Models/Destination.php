<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
class Destination extends Model implements HasMedia,TranslatableContract
{

    use HasMediaTrait,Translatable;
    ///images/09/11/1536685147.jpg
    protected static $defaultImage = '/images/09/11/1536685147.jpg';

    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
      $url = $this->getFirstMediaUrl($collectionName, $conversionName);

      return $url ? $url : $this::$defaultImage ?? '';
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(582)
            ->height(327);
    }

  	protected $fillable = ['destination_id'];

    public $translatedAttributes = ['name'];
    // protected $fillable = ['author'];

	public function packages()
	{
	    return $this->belongsToMany('App\Models\Package','destination_package', 'destination_id', 'package_id');
	}
    public function countryCategory()
    {
        return $this->belongsTo('App\Models\CountryCategory','country_category');
    }

    public function registerMediaCollections(){
        $this->addMediaCollection('destinationimage')->acceptsFile(function (File $file){
            return $file->mimeType === 'image/jpeg';
        });
    }

    public function scopeSearch($query, $request)
    { 

        if ($request->destination_name) {
            $query->where('name', 'LIKE', '%' . $request->destination_name . '%' );
        }         
        if ($request->description) {
            $query->where('description', 'LIKE', '%' . $request->description . '%' );
        }  
   
        return $query->select('destinations.*');     
    }


}
