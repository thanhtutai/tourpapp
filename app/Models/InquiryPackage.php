<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InquiryPackage extends Model
{
  // protected $table = 'checkavailablepackages';
	protected $fillable = ['package_id','destination_id','ph_num','first_name','last_name','messenger_user_id','chatfuel_user_id','quantity','email','tour_type','month_id','price_each','available','is_replied','user_id'];

  	public function package()
	{
	  return $this->belongsTo('App\Models\Package','package_id');
	}
}
