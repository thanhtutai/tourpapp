<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MonthTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
}
