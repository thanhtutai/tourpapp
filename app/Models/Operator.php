<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
   protected $fillable = ['operator_id','name','address','ph_num','email','location_latitude','location_longitude','user_id','review_link','rating','review','messenger_url'];

    public function packages()
    {
        return $this->hasMany('App\Models\Package', 'operator_id','package_id');
    }

	public function user()
	{
	  return $this->belongsTo('App\User','user_id');
	}

	public function scopeSearch($query, $request)
    { 

      if ($request->operator_name) {
        $query->where('name', 'LIKE', '%' . $request->operator_name . '%' );
      }  
      if ($request->address!='') {     
        $query->where('address', 'LIKE', '%' . $request->address . '%' );
      }             

      if ($request->email!='') {     
        $query->where('email', 'LIKE', '%' . $request->email . '%' );
      }          
     
      return $query->select('operators.*');     
    }
}
