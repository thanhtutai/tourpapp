<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
class Order extends Model implements HasMedia
{
	use SoftDeletes,HasMediaTrait;
    protected $dates = ['deleted_at'];
	  protected $primaryKey = 'order_id';
    protected $fillable = ['order_id','booking_code','name','ph_num','messenger_user_id','chatfuel_user_id','email','quantity','price_each','deposit_amt','approval','package_id','passport_img','status','payment_type','upload_slip','payment_deadline','user_id','sent_rate_package','is_rate_package'];
  	
  	protected static $defaultImage = '/images/09/11/1536685147.jpg';

    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
      $url = $this->getFirstMediaUrl($collectionName, $conversionName);

      return $url ? $url : $this::$defaultImage ?? '';
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(582)
            ->height(327);
    }
    
	public function package()
	{
	  return $this->belongsTo('App\Models\Package','package_id');
	}

  public function customer()
  {
    return $this->belongsTo('App\Models\Customer', 'messenger_user_id');
  }

  public function ratePackage()
  {
    return $this->hasMany('App\Models\RatePackage');
  }
}
