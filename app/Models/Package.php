<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Carbon\Carbon;
class Package extends Model implements HasMedia
{

    use HasMediaTrait;
    protected $primaryKey = 'package_id';
    protected static $defaultImage = '/images/09/11/1536685147.jpg';

    public function getFirstOrDefaultMediaUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
      $url = $this->getFirstMediaUrl($collectionName, $conversionName);

      return $url ? $url : $this::$defaultImage ?? '';
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(582)
	    ->height(327)
	    ->performOnCollections('packageimage');
    }
    
    protected $fillable = ['package_id','name','price','description','review_rate','currency_id','start_date','end_date','operator_id','tour_leader_name','inquiry','quantity','quantity_sold'];

    public function destinations()
    {
        return $this->belongsToMany('App\Models\Destination','destination_package', 'package_id', 'destination_id');
    }
    public function programs()
    {
        return $this->belongsToMany('App\Models\Program','program_package', 'package_id', 'program_id');
    }

    public function operator()
    {
        return $this->belongsTo('App\Models\Operator','operator_id','operator_id');
    }    
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency','currency_id');
    }

    public function scopeSearch($query, $request)
    { 

      if ($request->package_name) {
        $query->where('name', 'LIKE', '%' . $request->package_name . '%' );
      }  
      if ($request->price!='') {     
        $query->where('price', 'LIKE', '%' . $request->price . '%' );
      }             
      if ($request->description) {
        $query->where('description', 'LIKE', '%' . $request->description . '%' );
      }  
      if ($request->quantity!='') {     
        $query->where('quantity', 'LIKE', '%' . $request->quantity . '%' );
      }             
      if ($request->quantity_sold!='') {     
        $query->where('quantity_sold', 'LIKE', '%' . $request->quantity_sold . '%' );
      }       
      if ($request->destination_id!='') {     
        $query->join('destination_package','packages.package_id','=','destination_package.package_id');
        $query->where('destination_package.destination_id', $request->destination_id);
      }        
      if ($request->operator_id!='') {     
        $query->where('operator_id', $request->operator_id);
      }       
      if ($request->currency_id!='') {     
        $query->where('currency_id', $request->currency_id);
      }        
      if ($request->is_active!='') {  
        $now = Carbon::now();  
        if($request->is_active == "active"){
            $query->where('start_date', '>', $now );
        }else{
            $query->where('start_date', '<', $now );
        }
      } 
      if ($request->inquiry!='') {     
        $query->where('inquiry', $request->inquiry );
      }        
      if ($request->start_date!='') {   
      // dd($request);  
        $query->whereDate('start_date','=', $request->start_date );
      }       
      if ($request->end_date!='') {     
        $query->whereDate('end_date','=', $request->end_date );
      }              
      return $query->select('packages.*');     
    }



    public function scopePackageSearch($query, $request)
    { 

      $search_package = $request->search_package;
      $search_package_array = preg_split('/[\s]+/', $search_package);

      $package_name =  array_key_exists(0, $search_package_array);
      if ($package_name) {
        $query->where('name', 'LIKE', '%' . $search_package_array[0] . '%' );
      }                 

      $month_value =  array_key_exists(1, $search_package_array);
      if ($month_value) {

        $month_format = Carbon::parse($search_package_array[1])->format('m');
        $query->whereMonth('start_date',  $month_format);
      }  

      $date_value =  array_key_exists(2, $search_package_array);
      if ($date_value) {

        $date_format = Carbon::parse($search_package_array[2])->format('d');
        $query->whereDate('start_date', $date_format );
      }    

      $price_value =  array_key_exists(3, $search_package_array);
      if ($price_value) {
        $query->where('price', 'LIKE', '%' . $search_package_array[3] . '%' );
      }   
      return $query->select('packages.*');     
    }


}
