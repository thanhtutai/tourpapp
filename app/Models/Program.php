<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $fillable = ['program_id','name','description','package_id','start_time','end_time','filename'];

	public function packages()
	{
	    return $this->belongsToMany('App\Models\Package','destination_package', 'program_id', 'package_id');
	}
}
