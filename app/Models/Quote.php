<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
 	protected $fillable = ['package_id','date','quantity','ph_num','chatfuel_user_id','messenger_user_id','first_name','last_name'];

 	public function package()
	{
	  return $this->belongsTo('App\Models\Package','package_id');
	}
}
