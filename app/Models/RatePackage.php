<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatePackage extends Model
{
    protected $fillable = ['order_id','messenger_user_id','hotel_during_trip','meal','transportation','tour_leader','tour_guide','overall_service'];

    public function order()
	{
		return $this->belongsTo('App\Models\Order', 'order_id');
	}          

}
