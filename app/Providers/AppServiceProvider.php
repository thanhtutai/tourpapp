<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Domain\Contracts\{PackageInterface,OperatorInterface,OrderInterface,DestinationInterface,CustomerInterface};
use App\Domain\Repositories\{PackageRepository,OperatorRepository,OrderRepository,DestinationRepository,CustomerRepository};
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       // if ($this->app->environment() == 'local') {
       //  $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
       // }
        $this->app->bind(PackageInterface::class, PackageRepository::class);
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
        $this->app->bind(OperatorInterface::class, OperatorRepository::class);
        $this->app->bind(OrderInterface::class, OrderRepository::class);
        $this->app->bind(DestinationInterface::class, DestinationRepository::class);
    }
}
