<?php

namespace App\Providers;
use App\Models\Order;
use App\Policies\OrderPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
         Order::class => OrderPolicy::class

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
       $this->registerPolicies();

       Gate::define('update-order', function ($user, $orders) {
        // dd();
        return $user->id == $orders->user_id || $user->roles->pluck('name')->first() == "Admin";
       });       
       Gate::define('delete-order', function ($user, $orders) {
        // dd();
        return $user->id == $orders->user_id || $user->roles->pluck('name')->first() == "Admin";
       });       
       Gate::define('view-ownorder', function ($user, $orders) {
        // dd();
        return $user->id == $orders->user_id || $user->roles->pluck('name')->first() == "Admin";
       });
    }
}
