<?php

return [
    'prefix' => '',

    'channels' => [
        'slack' => 'https://hooks.slack.com/services/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',
        'mail' => 'blahblah@gmail.com',
    ],
    // Currently three notification channels supported
    // Those are database, slack and email
    'via' => ['slack']
];
