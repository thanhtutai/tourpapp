<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('destination_id');
            $table->date('travel_date');
            $table->string('guest_name');
            $table->integer('quantity');
            $table->integer('hotel_during_trip');
            $table->integer('meal');
            $table->integer('transportation');
            $table->integer('tour_leader');
            $table->integer('tour_guide');
            $table->integer('overall_service');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_packages');
    }
}
