<?php

return [
    'title' => 'Home Page Title - English',
    'welcome_text' => 'Hello :first_name, I’m TourPare, quick searcher of outbound package. I will going to ask you a few questions so that i can help you better.',
    'heading' => 'Home Page Heading - English',
    'show' => 'show me',
    'month_list' => 'Month List',
    'countries' => 'Countries',
    'quantity_question'=>":first_name, will you travel with your friend? How many in total?",
    'quantity_question_block'=>"English Quantity Question",
    'inquiry_question_block'=>"English Inquiry Question",
    'other_destination'=>"Other Destination",
    '1'=>"1",
    '2'=>"2",
    '3'=>"3",
    '4'=>"4",
    '5'=>"5",
    '6'=>"6",
    '7'=>"7",
    '8'=>"8",
    '9'=>"9",
    '10'=>"10",
];