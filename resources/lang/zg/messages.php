<?php

return [
    'welcome_text' => 'မဂၤလာပါ :first_name ေရ  :first_name သြားခ်င္တဲ့ ျပည္ပခရီးကိုုုု ျမန္ျမန္ရွာေပးမယ့္သူ “TourPare” ေလးပါ. :first_name ရဲ႕ စိတ္ကူးခရီးနဲ႕ အနီးစပ္ဆံုုုုးျဖစ္ဖိုုုု႕ ေအာက္ပါေမးခြန္းေလးေတြေျဖေပးပါဦးေနာ္.',
    'heading' => 'Home Page Heading - Spanish',
    'title' => 'Home Page Heading - Spanish',
    'show' => 'ျပပါ',
    'month_list' => 'လ မ်ား',
    'countries' => 'ႏိုင္ငံမ်ား',
    'quantity_question'=>":first_name, ဘယ္နွေယာက္ အတူတူ သြားမွာလဲ",
    'quantity_question_block'=>"Zawgyi Quantity Question",
    'inquiry_question_block'=>"Zawgyi Inquiry Question",
    'other_destination'=>"အျခားႏိုင္ငံမ်ား",
    '1'=>"၁",
    '2'=>"၂",
    '3'=>"၃",
    '4'=>"၄",
    '5'=>"၅",
    '6'=>"၆",
    '7'=>"၇",
    '8'=>"၈",
    '9'=>"၉",
    '0'=>"၀",
];