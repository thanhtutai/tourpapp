@extends('layouts.default')
@section('content')

<br>
<br>
<br>
<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('currency.update', $currency->id) }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="title">Currency Name:</label>
        <input type="text" name="denomination" id="title" class="form-control" value="{{ $currency->denomination }}">  
      </div>

      <div class="form-group"> 
        <form action="{{ route('currency.update', $currency->id) }}"
  >
        {{ csrf_field() }}
        {{ method_field("patch") }}
        <button type="submit" class="btn btn-primary">Update Info</button>
        </form>
      </div> 
    </form>
  </div>
</div>
  
  @stop