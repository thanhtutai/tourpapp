@extends('layouts.default')
@section('content')
<br>
<h2>Currency Info</h2>
<br>
<br>
<div class="row">              
			  <div class="col-md-6">
			    <form action="{{ route('currency.store')}}" method="POST">
			      {{ csrf_field() }}

				      <div class="form-group">
				        <label for="title">Currency Name:</label>
				        <input type="text" name="denomination" id="title" class="form-control" value="">     
				      </div>

              <div class="form-group pull-right">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Create
                      </button>  
                  </div>
              </div>
          </form>              
        </div> 
  <div class="col-md-6">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Action</th>
          <th>ID</th>
          <th>Currency Name</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        @foreach($currency as $row )
        <tr class="success">
              <td>  <a href="{{ route('currency.edit', $row->id) }}" class="btn btn-success btn-xs">Edit</a></td>
              <td>{{ $row->id }}</td>
              <td>{{ $row->denomination }}</td> 
              <td> 
                <form action="{{ route('currency.destroy',$row->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                </form>
              </td>  
        </tr>
        @endforeach
      </tbody>
    </table>  
      <nav>
       <ul class="pagination">
        <li><?php //echo $currency->render();?></li>
       </ul>
     </nav>
</div>
</div>
</div>
@stop