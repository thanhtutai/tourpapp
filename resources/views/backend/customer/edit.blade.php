@extends('layouts.default')

@section('content')
<h2>Edit Customer info</h2>
 
  <div class="col-md-6">
	<form action="{{ route('customer.update', $customer->messenger_user_id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
     
            <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', $customer->name, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>
        
	        <div class="form-group">
	            {!! Form::label('FB First Name') !!}
	            {!! Form::text('first_name', $customer->first_name, array('required','class'=>'form-control', 
	            'placeholder'=>' First Name')) !!}
	        </div>        

	        <div class="form-group">
	            {!! Form::label('FB Last Name') !!}
	            {!! Form::text('last_name', $customer->last_name, array('required','class'=>'form-control', 
	            'placeholder'=>' Last Name')) !!}
	        </div>
			
			<div class="form-group">
			    {!! Form::label('Email') !!}
			    {!! Form::text('email', $customer->email, array('required','class'=>'form-control', 
			    'placeholder'=>' Email')) !!}
			</div>

			<div class="form-group">
			    {!! Form::label('Phone') !!}
			    {!! Form::text('ph_num', $customer->ph_num, array('required','class'=>'form-control', 
			    'placeholder'=>' Phone Number')) !!}
			</div>

            <div class="form-group"> 
              <form action="{{ route('customer.update', $customer->messenger_user_id) }}">
                 {{ csrf_field() }}
                 {{ method_field("patch") }}
                 <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
            </div> 
      </div>
    </form>
@endsection