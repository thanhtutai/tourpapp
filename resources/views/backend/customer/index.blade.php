@extends('layouts.default')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Customers</h1>
    </div>
</div>
 <div class="table-responsive">
 
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Facebook Name</th>
            <th>Ph Number</th>
            <th>Email</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <tbody>
    @foreach($customers as $row )
        <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->name}}</td>
            <td>{{$row->first_name}} {{ $row->last_name}}</td>
            <td>{{$row->ph_num}}</td>
            <td>{{$row->email}}</td>
            <td><a href="{{ route('customer.edit', $row->messenger_user_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a></td>
			<td> 
                <form action="{{ route('customer.destroy',$row->messenger_user_id) }}" method="POST">
	                @csrf
	                @method('DELETE')
	                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                </form>
            </td>  
        </tr>
    @endforeach  
    </tbody>
</table>
  {{ $customers->links() }}
</div>
@endsection