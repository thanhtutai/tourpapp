@extends('layouts.default')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Destination Create Form</h1>
    </div>
</div>

<form action="{{ route('destination.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      
          <div class="form-group">
            {!! Form::label('Eng Name') !!}
            {!! Form::text('name', null, array('required','class'=>'form-control', 
            'placeholder'=>' Name')) !!}
          </div>          

          <div class="form-group">
            {!! Form::label('Zawgyi Name') !!}
            {!! Form::text('zg_name', null, array('required','class'=>'form-control', 
            'placeholder'=>' Name')) !!}
          </div>          


          <div class="form-group"> 
            <button type="submit" class="btn btn-success">Create Destination</button>
          </div>
</form>
    
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
    CKEDITOR.replace( 'descriptions' );
    // var x = document.getElementById("myTextarea").required;

    </script>
@endsection