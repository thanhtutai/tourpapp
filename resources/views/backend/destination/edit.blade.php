@extends('layouts.default')
@section('content')
<style type="text/css">
  .update-img{
    display: none;
  }
  .img-update{
        background: #ebb940;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    color: #212121;
    font-family: Montserrat-Bold,"Arial",sans-serif;
    /* font-size: 13px; */
    display: inline-block;
    padding: 9px 14px;
  }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".img-update").on("click", function(){
       $('.update-img').css('display','block');
    });
});
</script>
<h2>Edit Destination info</h2>
 
  <div class="col-md-6">
	<form action="{{ route('destination.update', $destination->id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
     
            <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', $destination->name, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>                        
            <div class="form-group">
              {!! Form::label('Zg Name') !!}
              {!! Form::text('zg_name', $destination->translate('zg')['name'], array('required','class'=>'form-control', 
              'placeholder'=>'Zg Name')) !!}
            </div>            

         

            <div class="form-group"> 
            <form action="{{ route('destination.update', $destination->id) }}">
               {{ csrf_field() }}
               {{ method_field("patch") }}
                <button type="submit" class="btn btn-primary">Update Info</button>
            </form>

      </div> 
      </div>
    </form>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'descriptions' );
</script>
@endsection