    @extends('layouts.default')

    @section('content')

    <div class="row">

        <div class="col-lg-12">

            <h1 class="page-header">Destinations</h1>

        </div>

    </div>

<div class="create">
<button class="btn btn-success"><a href="{{route('destination.create')}}">Create</a></button>
</div>
<br>

<div class="row">
 
        {!! Form::open(['url'=>route('destination.index'),'method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}

            <div class="form-group col-md-4"> 
                <input type="text" name="destination_name" placeholder="Name" value="{{request()->input('destination_name')}}" class="form-control"/>
            </div>
                       
            <div class="form-group col-md-4">
                <a href="{{ route('destination.index') }}" class="btn btn-flat btn-danger">Reset</a>
                <button class="btn btn-flat btn-primary">Search</button>
            </div>


        {!! Form::close() !!}
</div>
     <div class="table-responsive">
     
    <table class="table table-striped table-bordered table-hover">

        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Zawgyi Name</th>

                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>

        <tbody>
        @foreach($destinations as $row )
            <tr>

                <td class="counterCell"></td>
                <td>{{$row->name}}</td>
                <td>{{$row->translate('zg')['name']}}</td>
                <td>
                    <a href="{{ route('destination.edit', $row->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
                </td>
                <td>
                    <form action="{{ route('destination.destroy',$row->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-xs ">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $destinations->links() }}
    </div>

    @endsection