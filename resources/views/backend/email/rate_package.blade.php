
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	p{
		display: block;
	}
</style>
<body>
	<p>Dear Operator,</p>

	<p>You have a new review!</p>

	<p>Destination: 
	@foreach($rate_package->order->package->destinations as $destination)
		{!! $destination->name !!}, 
	@endforeach
	</p>
	<p>Travel Date: {!! $rate_package->order->package->start_date !!}</p>
	<p>Guest Name: {{ App\Models\Customer::where('messenger_user_id',$rate_package->order->messenger_user_id)->pluck('name')->first() }}</p>
	<p>Quantity: {!! $rate_package->order->quantity !!}</p>
	<p>Hotel During Trip: {!!$rate_package->hotel_during_trip !!}/5</p>
	<p>Meals: {!! $rate_package->meal!!}/5</p>
	<p>Transportation: {!! $rate_package->transportation!!}/5</p>
	<p>Tour Leader: {!! $rate_package->order->package->tour_leader_name!!} ({!! $rate_package->tour_leader !!}/5)</p>
	<p>Tour Guide: {!! $rate_package->tour_guide !!}/5</p>
	<p>Overall Service: {!! $rate_package->overall_service !!}/5</p>
	<p>Regards,<br>
	TourPare Team.</p>
</body>
</html>
