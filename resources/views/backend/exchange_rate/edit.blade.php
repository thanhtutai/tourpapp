@extends('layouts.default')
@section('content')

<div class="row">
  <div class="col-md-6">
  
    <form action="{{ route('exchange-rate.update', $ex_rate->id) }}" method="POST">
      {{ csrf_field() }}

      <div class="form-group">
        <label for="title"> Bank Info:</label>
        <input type="text" name="bank_info" id="title" class="form-control" value="{{$ex_rate->bank_info}}"> 
      </div>            
      <div class="form-group">
        <label for="title"> Exchange Rate:</label>
        <input type="text" name="ex_rate" id="title" class="form-control" value="{{$ex_rate->exchange_rate}}"> 
      </div>     

      <div class="">
      {!! Form::label(' Date') !!}
       <div class="controls" style="position: relative">
       <input type='text' value="{{Carbon\Carbon::parse($ex_rate->date)->format('Y-m-d')}}" name="date" required class="form-control datepicker" id='datetimepicker4' />
       </div>
     </div>

      <div class="form-group"> 
        <form action="{{ route('exchange-rate.update', $ex_rate->id) }}"
        >
          {{ csrf_field() }}
          {{ method_field("patch") }}
          <button type="submit" class="btn btn-primary">Update Info</button>
        </form>  
      </div> 
      </div>
    </form>
  </div>

</div>

<script type="text/javascript">

      $(function () {
                $('.datepicker').datetimepicker({
                   defaultDate:  new Date(),
                   format: 'YYYY-MM-DD'
                });
            });
</script>
 @endsection