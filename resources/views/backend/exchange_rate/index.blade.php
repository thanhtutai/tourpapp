@extends('layouts.default')
@section('content')
<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Exchange Rate</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- <div class="row">              
        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
          </div>
        @endif
        <div class="col-md-6">
          <form action="{{ route('exchange-rate.store')}}" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="title"> Bank Info:</label>
              <input type="text" name="bank_info" id="title" class="form-control" value=""> 
            </div>            
            <div class="form-group">
              <label for="title"> Exchange Rate:</label>
              <input type="text" name="ex_rate" id="title" class="form-control" value=""> 
            </div>     

            <div class="">
            {!! Form::label(' Date') !!}
             <div class="controls" style="position: relative">
             <input type='text' name="date" required class="form-control datepicker" id='datetimepicker4' />
             </div>
           </div>

            <div class="form-group pull-right">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Create
                      </button>
                  </div>
              </div>
          </form>          
      </div>  -->
     

  <div class="col-md-12">

    <table class="table table-bordered">
      <thead>
        <tr>
        <th>Action</th>
        <th>ID</th>
        <th>Banking Info</th>
        <th>Exchange Rate</th>
        <th>Date</th>
      </thead>
      <tbody>
      @foreach($ex_rates as $row )
        <tr class="success">
            <td>  <a href="{{ route('exchange-rate.edit', $row->id) }}" class="btn btn-success btn-xs">Edit</a></td>
            <td>{{ $row->id }}</td>
            <td>{{ $row->bank_info }}</td>   
            <td>{{ $row->exchange_rate }}</td>   
            <td>{{ $row->date }}</td>   
        </tr>
      @endforeach
      </tbody>
    </table>  
   
</div>
<script type="text/javascript">

      $(function () {
                $('.datepicker').datetimepicker({
                   defaultDate:  new Date(),
                   format: 'YYYY-MM-DD'
                });
            });
</script>
 @endsection