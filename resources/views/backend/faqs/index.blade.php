@extends('layouts.default')
@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Faqs</h1>

    </div>
</div>

<!-- /.row -->

<div class="create">
<button class="btn btn-success"><a href="{{route('faq.create')}}">Create</a></button>
</div>
<br>

 <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Question</th>
            <th>Answer</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
    </thead>

    <tbody>
    @foreach($faqs as $row )
        <tr>

            <td>{{$row->id}}</td>
            <td>{{$row->question}}</td>
            <td>{{$row->answer}}</td> 
            <td><a href="{{ route('faq.edit', $row->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a></td>
            <td>
                 <form action="{{ route('faq.destroy',$row->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-xs ">Delete</button>
                </form>
            </td>
            
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@endsection