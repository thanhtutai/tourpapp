 @extends('layouts.default')



@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Operator Create Form</h1>

    </div>
</div>

<form action="{{ route('operator.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      
            <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', null, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Address') !!}
              {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' =>('Address'), 'rows' => '3']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Email') !!}
              {!! Form::text('email', null, array('required','class'=>'form-control', 
              'placeholder'=>' Email')) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Phone') !!}
              {!! Form::text('ph_num', null, array('required','class'=>'form-control', 
              'placeholder'=>'083838383')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Rating') !!}
              {!! Form::text('rating', null, array('required','class'=>'form-control', 
              'placeholder'=>' 4.5')) !!}
            </div>            

            <div class="form-group">
              {!! Form::label('Rating') !!}
              {!! Form::text('review', null, array('required','class'=>'form-control', 
              'placeholder'=>' 4.5')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Review link') !!}
              {!! Form::text('review_link', null, array('required','class'=>'form-control', 
              'placeholder'=>' www.facebook.com')) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Messenger link') !!}
              {!! Form::text('messenger_url', null, array('required','class'=>'form-control', 
              'placeholder'=>'m.me/468251169884113')) !!}
            </div>

<!--             <div class="form-group">
              {!! Form::label('Choose Operator Account') !!}
              <?php $user = \App\User::all(); ?>
              <select name="user_id" required class="form-control" >
                <option value="">--- Select Operator Account---</option>
                @foreach ($user as $key => $value)
                 <option value="{{ $value->id }}">{{ $value->name }} ({{$value->roles->pluck('name')->first()}})</option>
                @endforeach
              </select>
            </div> -->
          
            <div class="form-group"> 
              <button type="submit" class="btn btn-success">Create Operator</button>
            </div>
    </form>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'address' );
    </script>
@endsection