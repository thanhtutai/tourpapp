@extends('layouts.default')

@section('content')
<h2>Edit Operator info</h2>
 
  <div class="col-md-6">
	<form action="{{ route('operator.update', $operator->id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
     
            <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', $operator->name, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>
            
            <div class="form-group">
              {!! Form::label('Address') !!}
              {!! Form::textarea('address', $operator->address, ['class' => 'form-control', 'placeholder' =>('Address'), 'rows' => '3']) !!}
            </div>
             
            <div class="form-group">
                {!! Form::label('Email') !!}
                {!! Form::text('email', $operator->email, array('required','class'=>'form-control', 
                'placeholder'=>' Email')) !!}
            </div>            
            <div class="form-group">
                {!! Form::label('Phone') !!}
                {!! Form::text('ph_num', $operator->ph_num, array('required','class'=>'form-control', 
                'placeholder'=>' 02929299299')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Rating') !!}
              {!! Form::text('rating', $operator->rating, array('required','class'=>'form-control', 
              'placeholder'=>' 4.5')) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Review') !!}
              {!! Form::text('review', $operator->review, array('required','class'=>'form-control', 
              'placeholder'=>' 4.5')) !!}
            </div>

            <div class="form-group">
              {!! Form::label('Review link') !!}
              {!! Form::text('review_link', $operator->review_link, array('required','class'=>'form-control', 
              'placeholder'=>' www.facebook.com')) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Messenger link') !!}
              {!! Form::text('messenger_url', $operator->messenger_url, array('required','class'=>'form-control', 
              'placeholder'=>'m.me/468251169884113')) !!}
            </div>

            <div class="form-group"> 
              <form action="{{ route('operator.update', $operator->id) }}">
                 {{ csrf_field() }}
                 {{ method_field("patch") }}
                 <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
            </div> 
      </div>
    </form>
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'address' );
    </script>
@endsection