@extends('layouts.default')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Operators</h1>
    </div>
</div>

<div class="create">
    <button class="btn btn-success"><a href="{{route('operator.create')}}">Create</a></button>
</div>
<br>
<div class="row">
 
        {!! Form::open(['url'=>route('operator.index'),'method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}

            <div class="form-group col-md-4"> 
                <input type="text" name="operator_name" placeholder="Name" value="{{request()->input('operator_name')}}" class="form-control"/>
            </div>
              
            <div class="form-group col-md-4"> 
                <input type="text" name="address" placeholder="Address" value="{{ request()->input('address') }}" class="form-control"/>
            </div>            

            <div class="form-group col-md-4"> 
                <input type="text" name="email" placeholder="Email" value="{{ request()->input('email') }}" class="form-control"/>
            </div>

            <div class="form-group col-md-4">
                <a href="{{ route('operator.index') }}" class="btn btn-flat btn-danger">Reset</a>
                <button class="btn btn-flat btn-primary">Search</button>
            </div>
            {!! Form::close() !!}
</div>

 <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Review </th>
            <th>Rating</th>
            <th>Phone</th>
            <th>Review Link</th>
            <!-- <th>Me</th> -->
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <tbody>
    @foreach($operators as $row )
        <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->name}}</td>
            <td>{!!$row->address!!}</td>
            <td>{!!$row->email!!}</td>
            <td>{{$row->review}}</td>
            <td>{{$row->rating}}</td>
            <td>{{$row->phone}}</td>
            <td>{{$row->review_link}}</td>
         
            <td><a href="{{ route('operator.edit', $row->id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a></td>
            <td>
                <form action="{{ route('operator.destroy',$row->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-xs ">Delete</button>
                </form>
            </td>       
        </tr>
    @endforeach
    </tbody>
</table>
{{ $operators->links() }}
</div>

@endsection