@extends('layouts.default')

@section('content')
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

<h2>Upload Invoice Image</h2>
<div class="container">
<div class="row">
    <div class="list-group">
        @foreach($order->getMedia('approve_invoice_img') as $file)
        <div class="list-group-item">
            <div class="media">
                <div class="media-left">
                    @if(starts_with($file->mime_type, 'image'))
                        <a href="{{ $file->getUrl() }}" target="_blank">
                            <img class="media-object" src="{{ $file->getUrl() }}" alt="{{ $file->name }}" width="200px">
                        </a>
                    @else
                        <span class="glyphicon glyphicon-file large-icon"></span>
                    @endif
                </div>
                <div class="media-body">
                    <div class="btn-group pull-right">
                        <form action="{{ route('invoice_img_destroy',[$order->order_id,$file->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                    </div>
                    <h4 class="media-heading">{{ $file->name }}</h4>
                    <p> 
                        <small>
                            {{ $file->human_readable_size }} |
                            {{ $file->mime_type }}
                        </small>
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
    <form action="{{ route('approveInvoiceImg.store', $order->order_id) }}" method="POST" enctype="multipart/form-data"> 
            {{ csrf_field() }}
        <div class="form-group"> 
          <input type="file" name="image" id="image">
        </div>
        <div class="form-group"> 
          <button type="submit" class="btn btn-success">Send</button>
        </div>
    </form>
</div>

<script type="text/javascript">
  Dropzone.options.mediaDropzone = {
    parallelUploads: 1,
    init: function () {
        this.on("queuecomplete", function (file) {
            location.reload();
        });
    }
};
</script>
@endsection