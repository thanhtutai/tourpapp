@extends('layouts.default')

@section('content')
<h2>Edit Order info</h2>
 
  <div class="col-md-6">
	<form action="{{ route('order.update', $order->order_id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
     
            <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', $order->customer->name, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>
            <div class="form-group">
              {!! Form::label('Phone') !!}
              {!! Form::text('ph_num', $order->customer->ph_num, array('required','class'=>'form-control', 
              'placeholder'=>' Phone')) !!}
            </div>            

            <div class="form-group">
              {!! Form::label('Quantity') !!}
              {!! Form::text('quantity', $order->quantity, array('required','class'=>'form-control', 
              'placeholder'=>' Phone')) !!}
            </div>
                         
            <div class="form-group">
              {!! Form::label('Price') !!}
              {!! Form::text('price_each', $order->price_each, array('required','class'=>'form-control', 
              'placeholder'=>' Phone')) !!}
            </div>
             
             <div class="form-group">
                {!! Form::label('Email') !!}
                {!! Form::text('email', $order->customer->email, array('required','class'=>'form-control', 
                'placeholder'=>' Email')) !!}
              </div>
              <select name="status">
                <option value="{!! $order->status !!}">{!! $order->status !!}</option>
                <option value="fully-paid">Fully Paid</option>
                <option value="deposited">Deposited</option>
                <option value="waiting-for-receipt">Waiting for receipt</option>
                
              </select>
              
              <div class="form-group"> 
              <form action="{{ route('order.update', $order->order_id) }}">
                 {{ csrf_field() }}
                 {{ method_field("patch") }}
              <button type="submit" class="btn btn-primary">Update Info</button>
              </form>
      </div> 
      </div>
    </form>

@endsection