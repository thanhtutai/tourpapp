@extends('layouts.default')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Orders</h1>
    </div>
</div>
 <div class="table-responsive">
 
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Order Id</th>
            <th>Booking Code</th>
            <th>Name</th>
            <th>Facebook Name</th>
            <th>Ph Number</th>
            <th>Email</th>
            <th>Quantity</th>
            <th>Package Name</th>
            <th>Price Each</th>
            <th>Deposit Amount</th>
            <th>Passport Image</th>
            <th>Slip Image</th>
            <th>Created at</th>
            <th>Payment Deadline</th>
            <th>Status</th>
            <th>Payment Type</th>
            <th>Approve Invoice</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <tbody>
    @foreach($orders as $row )
        <tr>
        @if(Gate::allows('view-ownorder', $row)) 
     
            <td>{{$row->order_id}}</td>
            <td>{{$row->booking_code}}</td>
            <td> {{$row->name}}</td>
            <td>@if($row->customer) {{$row->customer->first_name}} {{$row->customer->last_name}} @endif</td>
            <td>{{$row->ph_num}} </td>
            <td> {{$row->email}} </td>
            <td>{{$row->quantity}}</td>
            <td>@if($row->package){{$row->package->name}} @endif</td>
            <td>{{$row->price_each}}</td>
            <td>{{$row->deposit_amt}}</td>
            <td><img src="{{$row->passport_img}}" width="100px"></td>
            <td><img src="{{$row->upload_slip}}" width="100px"></td>
            <td>{{ Carbon\Carbon::parse($row->created_at)->format('Y-m-d')}}</td>
            <td>{{ Carbon\Carbon::parse($row->payment_deadline)->format('Y-m-d')}}</td>
            <td>{{$row->status}}</td>
            <td>{{$row->payment_type}}</td>
            <td>
                <a href="{{ route('order.approveInvoiceImg', $row->order_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
            </td>
            <td>
            @if(Gate::allows('update-order', $row)) 
                <a href="{{ route('order.edit', $row->order_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
            @endif
            </td>
            <td>
            @if(Gate::allows('delete-order', $row)) 
                <form action="{{ route('order.destroy',$row->order_id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-xs ">Delete</button>
                </form>
            @endif
            </td>
        @endif
        </tr>
    @endforeach  
    </tbody>
</table>
  {{ $orders->links() }}
</div>
@endsection
