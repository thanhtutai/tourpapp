@extends('layouts.default')



@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Order Detail</h1>

    </div>
</div>

<table class="table table-striped table-bordered table-hover">

    <thead>
        <tr>
            <th>#</th>
            <th>Order Id</th>
            <th>Name</th>
            <th>Messenger User Id</th>
            <th>Chatfuel User Id</th>
            <th>Ph Number</th>
            <th>Email</th>
            <th>Quantity</th>
            <th>Price Each</th>
            <th>Approval</th>
            <th>Approve</th>
            <th>Unapprove</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->order_id}}</td>
            <td>{{$order->name}}</td>
            <td>{{$order->messenger_user_id}}</td>
            <td>{{$order->chatfuel_user_id}}</td>
            <td>{{$order->ph_num}}</td>
            <td>{{$order->email}}</td>
            <td>{{$order->quantity}}</td>
            <td>{{$order->price_each}}</td>
            <td>@if ($order->approval == 1)
                <div class="text-success"> Approved</div>
                @else
                <div class="text-danger">Unapproved</div>
                @endif   
            </td>
             <td>
              <form action="{{ route('order.approve',$order->order_id) }}" method="POST">
                @csrf
                @method('PUT')
               <input class="hide" type="checkbox" name="approval" value="1" checked> 
               <button type="submit" class="btn btn-success">Approve</button>
               </form>
            </td>

            <td>
              <form action="{{ route('order.approve',$order->order_id) }}" method="POST">
                    @csrf
                    @method('PUT')
               <input type="checkbox"  class="hide" name="approval" value="0" checked> 
               <button type="submit" class="btn btn-danger">Unapprove</button>
               </form>
            </td>
        </tr>     
    </tbody>
</table>

@endsection