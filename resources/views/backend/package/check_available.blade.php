@extends('layouts.default')



@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Package inquiries</h1>

    </div>
</div>
 <div class="table-responsive"> 
  <table class="table table-striped table-bordered table-hover">

    <thead>

        <tr>
            <th>#</th>
            <th>Package Name</th>
            <th>Username</th>
            <th>Created Date</th>
            <th>Quantity</th>
            <th>Status</th>
            <th>Reply Stage</th>
            <th>Reply By</th>
            <!-- <th>Available</th> -->
            <!-- <th>Unavailable</th> -->
        </tr>
    </thead>
    <tbody>
   
        <tr>

            <td>{{$package->id}}</td>
            <td><?php $package_array = \App\Models\Package::where('package_id',$package->package_id)->first(); ?>{{ $package_array['name']}}</td>
            <!-- <td>{{$package->name}}</td> -->
            <td>{{$package->first_name}} {{$package->last_name}}</td>
            <td>{{Carbon\Carbon::parse($package->created_at)->format('g:i:s a / d-F-Y ')}}</td>
            <td>{{$package->quantity}}</td>

            <td>@if ($package->available == 1)
              <div class="text-success"> Available</div>
              @else
              <div class="text-danger">Unavailable</div>
              @endif   
            </td>

            <td>@if ($package->is_replied == 1)
              <div class="text-success"> Replied</div>
              @else
              <div class="text-danger">Un-reply yet</div>
              @endif   
            </td>  
            <td>
             {{App\User::where('id',$package->user_id)->first()['name']}}
            </td>
            <td>
              <form action="{{ route('check-package.update',$package->id) }}" method="POST">
                @csrf
                @method('PUT')
               <input class="hide" type="checkbox" name="available" value="1" checked> 
               <!-- <button type="submit" class="btn btn-success">Available</button> -->
               </form>
            </td>

            <td>
              <form action="{{ route('check-package.update',$package->id) }}" method="POST">
                @csrf
                @method('PUT')
               <input type="checkbox"  class="hide" name="available" value="0" checked> 
               <!-- <button type="submit" class="btn btn-danger">Unavailable</button> -->
               </form>
            </td>
          
        </tr>
   
    </tbody>

</table>
</div>
  

@endsection