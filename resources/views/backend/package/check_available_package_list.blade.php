@extends('layouts.default')
@section('content')

<style type="text/css">
  .hide{
    display: none;
  }
</style>
<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Package inquiries list</h1>

    </div>
</div>


 <div class="col-md-12">
  <div class="panel with-nav-tabs panel-info">
      <div class="panel-heading">
              <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab1info" data-toggle="tab">New inquiries</a></li>
                  <li><a href="#tab2info" data-toggle="tab">Old inquiries</a></li>
              </ul>
      </div>

  <div class="panel-body">
    <div class="tab-content">
     <div class="tab-pane fade in active" id="tab1info">
  
      <h2></h2>
      <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover">

       <thead>

        <tr>

            <th>#</th>
            <th>Package Name</th>
            <th>Username</th>
            <th>Created Date</th>
            <th>Quantity</th>
            <th>Phone</th>	    
            <th>Status</th>
            <th>Reply Stage</th>
<!--             <th>Available</th>
            <th>Unavailable</th> -->

        </tr>

    </thead>


    <tbody>
    @foreach($packages as $package )
       @if($package->is_replied == 0)
        <tr>

            <td class="counterCell"></td>
            <td><?php $package_array = \App\Models\Package::where('package_id',$package->package_id)->first(); ?>{{ $package_array['name']}}</td>
            <!-- <td>{{$package->name}}</td> -->
            <td>{{$package->first_name}} {{$package->last_name}}</td>
            <td>{{Carbon\Carbon::parse($package->created_at)->format('d-m-y')}}</td>
            <td>{{$package->quantity}}</td>
            <td>{{$package->ph_num}}</td>
            <td>@if ($package->available == 1)
              <div class="text-success"> Available</div>
              @else
              <div class="text-danger">Unavailable</div>
              @endif   
            </td>

            <td>
              {{App\User::where('id',$package->user_id)->first()['name']}}
            </td>
            <td>
            @if(isset($package_array['name']))
              <form action="{{ route('check-package.update',$package->id) }}" method="POST">
                @csrf
                @method('PUT')
               <input class="hide" type="checkbox" name="available" value="1" checked> 
               <!-- <button type="submit" class="btn btn-success">Available</button> -->
               </form>
            @endif
            </td>

            <td>
              <form action="{{ route('check-package.update',$package->id) }}" method="POST">
                @csrf
                @method('PUT')
               <input type="checkbox"  class="hide" name="available" value="0" checked> 
<!--                <button type="submit" class="btn btn-danger">Unavailable</button> -->
               </form>
            </td>
          
        </tr>
        @endif
    @endforeach
       
    </tbody>

    </table>
    </div>
   <!-- test -->
    </div>

  <div class="tab-pane fade" id="tab2info">
  <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
            <th>#</th>
            <th>Package Name</th>
            <th>Username</th>
            <th>Created Date</th>
            <th>Quantity</th>
            <th>Status</th>
            <th>Reply By</th>
           

        </tr>

    </thead>


    <tbody>
    @foreach($packages as $package )
    @if($package->is_replied == 1)
        <tr>

            <td>{{$package->id}}</td>
            <td><?php $package_array = \App\Models\Package::where('package_id',$package->package_id)->first(); ?>{{ $package_array['name']}}</td>
            <!-- <td>{{$package->name}}</td> -->
            <td>{{$package->first_name}} {{$package->last_name}}</td>
            <td>{{Carbon\Carbon::parse($package->created_at)->format('d-m-y ')}}</td>
            <td>{{$package->quantity}}</td>

            <td>@if ($package->available == 1)
              <div class="text-success"> Available</div>
              @else
              <div class="text-danger">Unavailable</div>
              @endif   
            </td>

            <td>
              {{App\User::where('id',$package->user_id)->first()['name']}}
            </td>
          
          
        </tr>
        @endif
      @endforeach
       
      </tbody>
    </table>
        </div>   
        </div>
      </div>
    </div>
  </div>
</div>
 {{ $packages->links() }}

@endsection
