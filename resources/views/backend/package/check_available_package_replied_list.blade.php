@extends('layouts.default')
@section('content')

<style type="text/css">
  .hide{
    display: none;
  }
</style>
<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Package inquiries list</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->
 <div class="table-responsive">
 
<table class="table table-striped table-bordered table-hover">

    <thead>

        <tr>

            <th>#</th>
            <th>Package Name</th>
            <th>Username</th>
            <th>Created Date</th>
            <th>Quantity</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Reply Stage</th>
            <th>Reply By</th>
          

        </tr>

    </thead>


    <tbody>
    @foreach($packages as $package )
        <tr>

            <td class="counterCell"></td>
            <td><?php $package_array = \App\Models\Package::where('package_id',$package->package_id)->first(); ?>{{ $package_array['name']}}</td>
            <!-- <td>{{$package->name}}</td> -->
            <td>{{$package->first_name}} {{$package->last_name}}</td>
            <td>{{Carbon\Carbon::parse($package->created_at)->format('g:i:s a / d-F-Y ')}}</td>
            <td>{{$package->quantity}}</td>
                      <td>{{$package->ph_num}}</td>
            <td>@if ($package->available == 1)
              <div class="text-success"> Available</div>
              @else
              <div class="text-danger">Unavailable</div>
              @endif   
            </td>

            <td>@if ($package->is_replied == 1)
              <div class="text-success"> Replied</div>
              @else
              <div class="text-danger">Un-reply yet</div>
              @endif   
            </td>  
            <td>
              {{App\User::where('id',$package->user_id)->first()['name']}}
            </td>
           
          
        </tr>
    @endforeach
       
    </tbody>

</table>
</div>
{{ $packages->links() }}
  

@endsection
