 @extends('layouts.default')



@section('content')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<style type="text/css">
/*  .quantity{
    display: none;
  }*/
</style>
<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Package Create Form</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

  @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
    </div>
  @endif
<form action="{{ route('package.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      
          <div class="form-group">
            {!! Form::label('Name') !!}
            {!! Form::text('name', null, array('required','class'=>'form-control', 
            'placeholder'=>' Name')) !!}
          </div>          
      
	        <div class="form-group">
          {!! Form::label('Package Summary') !!}
          {{Form::file('package_detail_img[]', ['multiple' => 'multiple'])}}
          </div>
  


<!--           <div class="form-group">
            {!! Form::label('Tour Summary') !!}
            {!! Form::textarea('descriptions', null, array('required','class'=>'form-control', 
            'placeholder'=>'Description')
            ) !!}
          </div>
 -->
      	  <div class="form-group">
                <input type="checkbox" name="inquiry" value="1" checked> Inquiry
                </div>
          <div class="form-group quantity" id="quantity">
        <!--                 {!! Form::label('Quantity') !!}
                    {!! Form::number('quantity', null, array('required','class'=>'form-control', 
                    'placeholder'=>' Quantity')) !!}
                  </div>
        -->	 

         <div class="form-group">
            {!! Form::label('Price') !!}
            {!! Form::number('price', null, array('required','class'=>'form-control', 
            'placeholder'=>'Price')) !!}
          </div>

          <label for="photo">
          {!! Form::label('Package Image to show in messenger') !!}
            {!! Form::file('image') !!}
          </label>
          
          <div class="">
            {!! Form::label('Start Date') !!}
             <div class="controls" style="position: relative">
             <input type='text' name="start_date" required class="form-control datepicker" id='datetimepicker4' />
             </div>
           </div>
           
            <div class="controls" style="position: relative">  
            {!! Form::label('End Date') !!}
            <input type='text' name="end_date" class="form-control datepicker" required="" id='datetimepicker5' />
          </div>
          
          <div class="form-group">
           {!! Form::label('Choose Operator') !!}
              <?php $operator = \App\Models\Operator::all(); ?>
              <select name="operator_id" required class="form-control" >
                <option value="">--- Select Operator ---</option>
                @foreach ($operator as $key => $value)
                <option value="{{ $value->operator_id }}">{{ $value->name }}</option>
                @endforeach
              </select>
          </div>          
        <!--   <div class="form-group">
            {!! Form::label('Tour Leader Name') !!}
            {!! Form::text('tour_leader_name', null, array('required','class'=>'form-control', 
            'placeholder'=>' Tour Leader Name')) !!}
          </div> -->
          <div class="form-group">
           {!! Form::label('Choose Currency') !!}
              <?php $currency = \App\Models\Currency::all(); ?>
              <select name="currency_id" required class="form-control" >
                <option value="">--- Select currency ---</option>
                @foreach ($currency as $key => $value)
                <option value="{{ $value->id }}">{{ $value->denomination }}</option>
                @endforeach
              </select>
          </div>
          
         <!--  <div class="form-group">
            <input type="checkbox" name="currency_id" value="1" checked> Currency
          </div> -->
            
          <?php $destination = \App\Models\Destination::all(); ?>
          <div class="table-responsive">  
              <table class="table table-bordered" id="dynamic_field2">  
                  <tr>  
                      <td><select name="destination[]" required class="form-control" >
                      <option value="">--- Select Destination ---</option>
                      @foreach ($destination as $key => $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                      @endforeach
                      </select></td>  
                      <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                  </tr>  
              </table>  
          </div> 

<!--           <div class="table-responsive">  
            <table class="table table-bordered" id="dynamic_field3">  
                <tr>  
                  <td> 
                    {!! Form::text('program_name[]', null, array('class'=>'form-control', 
                    'placeholder'=>' Program Name')) !!}
                  </td>          
                  <td>  {!! Form::textarea('program_description[]', null, ['class' => 'form-control', 'placeholder' =>('Description'), 'rows' => '3']) !!}</td> 
               
                  <td><input type="datetime-local" name="program_start_date"></td>      
                   <td><button type="button" name="add" id="add2" class="btn btn-success">Add More</button></td>
                </tr>                  
           </table>       
          </div>    -->


      <div class="form-group"> 
        <button type="submit" class="btn btn-success">Create Package</button>
      </div>
    </form>
<!--   <script type="text/javascript">
  $(document).ready(function(){
    $(function()
    {
      $('[name="inquiry"]').change(function()
      {
        if ($(this).is(':checked')) {
           // Do something...
          document.getElementById('quantity').style.display = 'none'
        }else{
           // alert('unchecked...');
           document.getElementById('quantity').style.display = 'block'
        };
      });
    });
   }); 
  </script> -->

<script type="text/javascript">
  $(document).ready(function(){
    //set default for usd
    $('select option:contains("USD")').prop('selected',true);
    //group add limit
    var maxGroup = 10;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "<?php echo url('addmore'); ?>";
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field2').append('<tr id="row'+i+'" class="dynamic-added"><td><select name="destination[]" required class="form-control" ><option value="">--- Select Destination ---</option> @foreach ($destination as $key => $value)<option value="{{ $value->id }}">{{ $value->name }}</option>@endforeach</select></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr> ');  
      });      

      $('#add2').click(function(){  
           i++;  
           $('#dynamic_field3').append('<tr id="row'+i+'" class="dynamic-added"><td>{!!Form::text("program_name[]", null, array("required","class"=>"form-control","placeholder"=>"Program Name"))!!}</td><td> {!! Form::textarea("program_description[]", null,["class" => "form-control", "placeholder" =>("Description"), "rows "=> "3"]) !!}</td><td><input type="datetime-local" name="program_start_date"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr> ');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                    }
                }  
           });  
      });  


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });  
</script>
<script type="text/javascript">

      $(function () {
                $('.datepicker').datetimepicker({
                   defaultDate:  new Date(),
                   format: 'YYYY-MM-DD'
                });
            });
</script>
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
    CKEDITOR.replace( 'descriptions' );
    </script>
@endsection
