@extends('layouts.default')

@section('content')
<style type="text/css">
    #page-wrapper{
        min-height: 2000px !important;
    }

  .update-img{
    display: none;
  }
  .img-update{
        background: #ebb940;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    color: #212121;
    font-family: Montserrat-Bold,"Arial",sans-serif;
    /* font-size: 13px; */
    display: inline-block;
    padding: 9px 14px;
  }
/*  .quantity{
    display: none;
  }*/
</style>
<style type="text/css">
  .quantity{
    /*display: none;*/
  }
</style>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".img-update").on("click", function(){
       $('.update-img').css('display','block');
    });
});
</script>
<h2>Edit Operator info</h2>


  @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
    </div>
    @endif
    
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Package Summary</h3>
    </div>
    <div class="panel-body">
        <form action="{{ route('package_image.store', $package->package_id) }}"
              class="dropzone"
              id="media-dropzone">

            {{ csrf_field() }}

        </form>
    </div>
</div>
<script type="text/javascript">
  Dropzone.options.mediaDropzone = {
    parallelUploads: 1,
    init: function () {
        this.on("queuecomplete", function (file) {
            location.reload();
        });
    }
};
</script>

    <div class="list-group">

   @foreach($package->getMedia('package_detail_img') as $file)

            <div class="list-group-item">
                <div class="media">
                    <div class="media-left">
                        @if(starts_with($file->mime_type, 'image'))
                     
                            <a href="{{ $file->getUrl() }}" target="_blank">
                                <img class="media-object" src="{{ $file->getUrl() }}" alt="{{ $file->name }}" width="200px">
                                    </a>
                        @else
                            <span class="glyphicon glyphicon-file large-icon"></span>
                        @endif
                    </div>
                    <div class="media-body">
                        <div class="btn-group pull-right">
                              <form action="{{ route('destroy',[$package->package_id,$file->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
            </form>

                           
                        </div>
                        <h4 class="media-heading">{{ $file->name }}</h4>
                        <p> <small>
                                {{ $file->human_readable_size }} |
                                {{ $file->mime_type }}
                            </small>
                        </p>

      
                    </div>
                </div>
            </div>
        @endforeach
    </div>

  <div class="col-md-12">
	<form action="{{ route('package.update', $package->package_id) }}" enctype="multipart/form-data" method="POST">
      {{ csrf_field() }}
       <div class="form-group">
              {!! Form::label('Name') !!}
              {!! Form::text('name', $package->name, array('required','class'=>'form-control', 
              'placeholder'=>' Name')) !!}
            </div>
            <div class="form-group">
              <input type="checkbox" name="inquiry" value="1" @if($package->inquiry == 1) checked @endif> Inquiry

             </div>
<!--     
          <div class="form-group">
            {!! Form::label('Tour Summary') !!}
            {!! Form::textarea('descriptions', $package->description, array('required','class'=>'form-control', 
            'placeholder'=>'Description')
            ) !!}
          </div>    -->       
    <!--   
           <div class="form-group @if($package->inquiry == 1) quantity @endif" id="quantity">
              {!! Form::label('Quantity') !!}
              {!! Form::number('quantity',  $package->quantity, array('0','class'=>'form-control', 
              'placeholder'=>' Quantity')) !!}
            </div>
  -->    
            <div class="form-group">
              {!! Form::label('Price') !!}
              {!! Form::text('price', $package->price, array('required','class'=>'form-control', 
              'placeholder'=>'Price')) !!}
            </div>

            <div class="form-group">
            <?php if(!empty($package->getMedia('packageimage')->first()))
              {?>
            <img width="200px" src="{{$package->getFirstMediaUrl('packageimage')}}">
            <?php }?>
              <div class="img-update">Update Img</div>
            </div>
            
            <label for="photo" class="update-img">
             {!! Form::file('image') !!}
            </label>


<!--              <div class="form-group">
              {!! Form::label('Description') !!}
              {!! Form::textarea('descriptions', $package->description, ['class' => 'form-control', 'placeholder' =>('Description'), 'rows' => '3']) !!}
             </div> -->
        
             <div class="">
                <div class="controls" style="position: relative">
                {!! Form::label('Start Date') !!}
                <input type='text' name="start_date" required class="form-control datepicker" value="{{Carbon\Carbon::parse($package->start_date)->format('Y-m-d')}}" id='datetimepicker4' />
                </div>
             </div>

             <div class="controls" style="position: relative">
                {!! Form::label('End Date') !!}
                <input type='text' name="end_date" class="form-control datepicker" required="" value="{{Carbon\Carbon::parse($package->end_date)->format('Y-m-d')}}" id='datetimepicker5' />
             </div>             
             <div class="form-group">
                  <label for="title">Choose Operator</label>
                 <?php $operator = \App\Models\Operator::all(); ?>
                  <select id="operator_id" name="operator_id" class="form-control select2">
                      <option value="{{ $package->operator_id }}">{{ $package->operator['name'] }}</option>
                      @foreach($operator as $row)
                          <option value="{{ $row->operator_id }}">
                              {{ $row->name }} 
                          </option>
                      @endforeach
                  </select>
               </div>
<!--                <div class="form-group">
                {!! Form::label('Tour Leader Name') !!}
                {!! Form::text('tour_leader_name', $package->tour_leader_name, array('required','class'=>'form-control', 
                'placeholder'=>' Tour Leader Name')) !!}
               </div> -->
               <div class="form-group">
                  <label for="title">Choose Currency</label>
                 <?php $currency = \App\Models\Currency::all(); ?>
                  <select id="currency_id" name="currency_id" class="form-control select2">
                      <option value="{{ $package->currency_id }}">{{ $package->currency['denomination'] }}</option>
                      @foreach($currency as $row)
                          <option value="{{ $row->id }}">
                              {{ $row->denomination }} 
                          </option>
                      @endforeach
                  </select>
               </div>

               <label>Destination</label>
               <?php $destination_list = \App\Models\Destination::all(); ?>
            <tr><button type="button" name="add" id="add" class="btn btn-success">Add More</button></tr>  
            <div class="table-responsive">  
                <table class="table table-bordered" id="dynamic_field2">  
                     @foreach($package->destinations as $destination)
                    <tr id="row{{$destination->id}}{{$destination->id}}">  
                        <td>

                        <select name="destination[]" required class="form-control" >
                        <option value="{{ $destination->destination_id }}">{{ $destination->name }}</option>
                        @foreach ($destination_list as $key => $value)
                          <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                        </select>

                        </td>  
                      <td><button type="button" name="remove" id="{{$destination->id}}{{$destination->id}}" class="btn btn-danger btn_remove">X</button></td>
                    </tr>  

                    @endforeach
                     
                </table>  
            </div> 

            <?php $program_list = \App\Models\Program::where('package_id',$package->package_id)->get(); 
            ?>
<!--             <label>Programs</label>   <tr><button type="button" name="add" id="add2" class="btn btn-success">Add Program</button></tr>  
            <div class="table-responsive">  
                <table class="table table-bordered" id="dynamic_field3">  
                    @foreach($program_list as $program)       
                    <tr id="row{{$program->id}}{{$program->id}}">  
                      <td>
                          {!! Form::text('program_name[]', $program->name, array('','class'=>'form-control','placeholder'=>' Name')) !!}
                          {!! Form::textarea('program_description[]', $program->description, ['class' => 'form-control', 'placeholder' =>('Description'), 'rows' => '3']) !!}
                          <input type="datetime-local" value="{{Carbon\Carbon::parse($program->start_date)->format('Y-m-d\TH:i:s')}}" name="program_start_date">
                        </td>  
                      <td>
                      <button type="button" name="remove" id="{{$program->id}}{{$program->id}}" class="btn btn-danger btn_remove">X</button></td>
                    </tr>  
                    @endforeach
                   
                </table>  
            </div>  -->

              <div class="form-group"> 
                <form action="{{ route('package.update', $package->package_id) }}">
                   {{ csrf_field() }}
                   {{ method_field("patch") }}
                <button type="submit" class="btn btn-primary">Update Info</button>
                </form>
              </div>
    </form>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'descriptions' );
</script>
  
<script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize         :       1,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };
</script>
<script type="text/javascript">
  $(document).ready(function(){
    //set default for usd
    // $('select option:contains("USD")').prop('selected',true);
    //group add limit
    var maxGroup = 10;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){      
      var postURL = "<?php echo url('addmore'); ?>";
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field2').append('<tr id="row'+i+'" class="dynamic-added"><td><select name="destination[]" required class="form-control" ><option value="">--- Select Destination ---</option> @foreach ($destination_list as $key => $value)<option value="{{ $value->id }}">{{ $value->name }}</option>@endforeach</select></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr> ');  
      });      

      $('#add2').click(function(){  
           i++;  
           $('#dynamic_field3').append('<tr id="row'+i+'" class="dynamic-added"><td>{!!Form::text("program_name[]", null, array("required","class"=>"form-control","placeholder"=>"Program Name"))!!}</td><td> {!! Form::textarea("program_description[]", null,["class" => "form-control", "placeholder" =>("Description"), "rows "=> "3"]) !!}</td><td><input type="datetime-local" name="program_start_date"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr> ');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


      $('#submit').click(function(){            
           $.ajax({  
                url:postURL,  
                method:"POST",  
                data:$('#add_name').serialize(),
                type:'json',
                success:function(data)  
                {
                    if(data.error){
                        printErrorMsg(data.error);
                    }else{
                        i=1;
                        $('.dynamic-added').remove();
                        $('#add_name')[0].reset();
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-error-msg").css('display','none');
                        $(".print-success-msg").find("ul").append('<li>Record Inserted Successfully.</li>');
                    }
                }  
           });  
      });  


      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }
    });  
</script>
<!-- 
<script type="text/javascript">
  
    $(function()
    {
      $('[name="inquiry"]').change(function()
      {
        if ($(this).is(':checked')) {
           // Do something...
          document.getElementById('quantity').style.display = 'none'
        }else{
           // alert('unchecked...');
           document.getElementById('quantity').style.display = 'block'
        };
      });
    });
  


  </script> -->
  <script type="text/javascript">

      $(function () {
                $('.datepicker').datetimepicker({
                   defaultDate:  new Date(),
                   format: 'YYYY-MM-DD'
                });
            });
</script>
@endsection

