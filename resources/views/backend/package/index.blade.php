@extends('layouts.default')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Packages</h1>
    </div>
</div>

<div class="create"><a href="{{route('package.create')}}"><button class="btn btn-success">Create</button></a></div><br>

<div class="row">
 
        {!! Form::open(['url'=>route('package.index'),'method'=>'GET', 'class'=>'form', 'id'=>'search_data']) !!}

            <div class="form-group col-md-3 col-xs-6"> 
               {!! Form::label('Name') !!}
                <input type="text" name="package_name" placeholder="Name" value="{{request()->input('package_name')}}" class="form-control"/>
            </div>
            
            <div class="col-md-3 col-xs-6 form-group">
               {!! Form::label('Active Package') !!}
                <select name="is_active" class="form-control">
                  <option value="">---Choose Active---</option>
                  <option value="active" {{(request()->input('is_active') == "active")?'selected':''}}>Active Packages</option>
                  <option value="expire" {{(request()->input('is_active') == "expire")?'selected':''}}>Expire Packages</option>
                </select>
            </div>      

            <div class="col-md-3 col-xs-6 form-group">
                {!! Form::label('Inquiry') !!}
                <select name="inquiry" class="form-control">
                  <option value="">---Inquiry---</option>
                  <option value="1" {{(request()->input('inquiry') == "1")?'selected':''}}>Yes</option>
                  <option value="0" {{(request()->input('inquiry') == "0")?'selected':''}}>No</option>
                </select>
            </div>
            <div class="col-md-3 col-xs-6 form-group">
                {!! Form::label('Start Date') !!}
                <input type='text' value="{{ request()->input('start_date') }}" name="start_date" class="form-control datepicker" id='datetimepicker4' />     
            </div>            
            <div class="col-md-3 col-xs-6 form-group">
                {!! Form::label('End Date') !!}
                <input type='text' name="end_date" value="{{ request()->input('end_date') }}" class="form-control datepicker" id='datetimepicker4' />     
            </div>

            <div class="form-group col-md-3 col-xs-6">
                {!! Form::label('Choose Operator') !!}
              <?php $operator = \App\Models\Operator::all(); ?>
              <select name="operator_id" class="form-control" >
                <option value="">--- Select Operator ---</option>
                @foreach ($operator as $key => $value)
                <option value="{{ $value->operator_id }}" {{(request()->input('operator_id') == $value->operator_id)?'selected':''}}>{{ $value->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-3 col-xs-6">
                {!! Form::label('Choose Destination') !!}
              <?php $destination = \App\Models\Destination::all(); ?>
              <select name="destination_id" class="form-control" >
                <option value="">--- Select Destination ---</option>
                @foreach ($destination as $key => $value)
                <option value="{{ $value->destination_id }}" {{(request()->input('destination_id') == $value->destination_id)?'selected':''}}>{{ $value->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-3 col-xs-6">
                {!! Form::label('Choose Currency') !!}
                <?php $currency = \App\Models\Currency::all(); ?>
                <select name="currency_id" class="form-control" >
                  <option value="">--- Select Currency ---</option>
                  @foreach ($currency as $key => $value)
                  <option value="{{ $value->id }}" {{(request()->input('currency_id') == $value->id)?'selected':''}}>{{ $value->denomination }}</option>
                  @endforeach
                </select>
            </div>  
            <div class="form-group col-md-3 col-xs-6">
              
                <a href="{{ route('package.index') }}" class="btn btn-flat btn-danger">Reset</a>
                <button class="btn btn-flat btn-primary">Search</button>
            </div>
        {!! Form::close() !!}
</div>
  <div class="table-responsive">
 
<table class="table table-striped table-bordered table-hover">

    <thead>

        <tr>
            <th>#</th>  
            <th>Name</th>
            <th>Price</th>
            <th>Destination</th>

            <!-- <th>Review Rate</th> -->
            <th> Inquiry</th>
            <th>Currency Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Operator Name</th>   
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <tbody>
    @foreach($packages as $row )
        <tr>
            <td class="counterCell"></td>
            <td>{{$row->name}}</td>
            <td>{{$row->price}}</td>

            <td>@foreach($row->destinations as $destination)
                 {{$destination->name}}
                @endforeach
            </td>


            <td>@if($row->inquiry == 1) Yes @else No @endif</td>
            <td><?php if(!empty($row->currency)){ ?> 
                    {{$row->currency->denomination}}
                <?php }?>
            </td>
            <td>{{$row->start_date}}</td>
            <td>{{$row->end_date}}</td>
            <td><?php if(!empty($row->operator)){ ?> 
                  {{$row->operator->name}}
                <?php }?>
            </td>           
            <td><a href="{{ route('package.edit', $row->package_id) }}" class="btn btn-warning btn-xs action-item" target="_blank"><i class="glyphicon glyphicon-edit"></i></a>
            </td>
            <td> 
            <form action="{{ route('package.destroy',$row->package_id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
            </form>
            </td>
        </tr>
    @endforeach
       
    </tbody>

</table>
{{ $packages->links() }}

</div>
<script type="text/javascript">

      $(function () {
                $('.datepicker').datetimepicker({
                   // defaultDate:  new Date(),
                   format: 'YYYY-MM-DD'
                });
            });
</script>
@endsection