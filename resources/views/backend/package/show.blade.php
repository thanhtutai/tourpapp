@extends('layouts.default')



@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Package Detail to reply user</h1>

    </div>
</div>

<table class="table table-striped table-bordered table-hover">

    <thead>
        <tr>
            <th>#</th>
            <th>Order Id</th>
            <th>Name</th>
            <th>Messenger User Id</th>
            <th>Chatfuel User Id</th>
            <th>Ph Number</th>
            <th>Email</th>
            <th>Quantity</th>
            <th>Price Each</th>
            <th>Approval</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>   
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->order_id}}</td>
            <td>{{$order->name}}</td>
            <td>{{$order->messenger_user_id}}</td>
            <td>{{$order->chatfuel_user_id}}</td>
            <td>{{$order->ph_num}}</td>
            <td>{{$order->email}}</td>
            <td>{{$order->quantity}}</td>
            <td>{{$order->price_each}}</td>
            <td>@if ($order->approval == 1)
               <div class="text-success"> Approved</div>
                @else
               <div class="text-danger">Unapproved</div>
               @endif  
            </td>
            <td>
                      <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal{{$order->order_id}}">Action</button>


            <!-- Modal -->
            <div class="modal fade" id="myModal{{$order->order_id}}" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                   
             <form action="{{ route('order.update',$order->order_id) }}" method="POST">
                  @csrf
                  @method('PUT')
                    <input type="checkbox" name="approval" value="1"> Approve<br>
                   <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
                
              </div>
            </div>

            </td>
        </tr>

    </tbody>
</table>

  

@endsection