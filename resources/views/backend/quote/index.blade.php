@extends('layouts.default')

@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Request Quotes</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

<!-- <div class="create"><button class="btn btn-"><a href="/order/create">Create</a></button></div> -->
 <div class="table-responsive">
 
<table class="table table-striped table-bordered table-hover">

    <thead>

        <tr>
         
            <th> Id</th>
            <th>Package Name</th>
            <th>Request Date</th>
            <th>Quantity</th>
            <th>Phone</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Created at</th>

        </tr>

    </thead>

    <tbody>
    @foreach($quotes as $row )
        <tr>
            <td>{{$row->id}}</td>
            <td>@if($row->package){{$row->package->name}} @endif</td>
            <td>{{$row->date}}</td>
            <td>{{$row->quantity}}</td>
            <td>{{$row->ph_num}}</td>
            <td>{{$row->first_name}}</td>
            <td>{{$row->last_name}}</td>
            <td>{{$row->created_at}}</td>
        </tr>
    @endforeach
       
    </tbody>

</table>
  {{ $quotes->links() }}
</div>
@endsection