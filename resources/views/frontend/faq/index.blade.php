@extends('frontend_layouts.headers')


@section('content')
<style type="text/css">
	.phpdebugbar{
		display: none;
	}
</style>
	<main>
		<section class="hero_in general">
			<div class="wrapper">
				<div class="container">
					<h1 class="fadeInUp"><span></span>Faq Section</h1>
				</div>
			</div>
		</section>
		<!--/hero_in-->
				
				<div class="col-lg-12" id="faq">
					<!-- <h4 class="nomargin_top">Payments</h4> -->
					<div role="tablist" class="add_bottom_45 accordion_2" id="payment">
					@foreach($faqs as $faq)

						<div class="card">
							<div class="card-header" role="tab">
								<h5 class="mb-0">
									<a data-toggle="collapse" href="#collapseOne_payment-{{$faq->id}}" aria-expanded="true"><i class="indicator ti-minus"></i>{{$faq->question}}</a>
								</h5>
							</div>

							<div id="collapseOne_payment-{{$faq->id}}" class="collapse" role="tabpanel" data-parent="#payment">
								<div class="card-body">
									<p>{{$faq->answer}}</p>
								</div>
							</div>
						</div>
						@endforeach
						<!-- /card -->
					</div>
				
					
							
						</div>
						<!-- /card -->
					</div>
		
		@endsection