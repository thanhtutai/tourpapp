<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.ansonika.com/panagea/tour-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 Aug 2018 14:11:16 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Panagea - Premium site template for travel agencies, hotels and restaurant listing.">
    <meta name="author" content="Ansonika">
    <title>Panagea | Premium site template for travel agencies, hotels and restaurant listing.</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="/frontend/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="/frontend/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/frontend/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/frontend/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/frontend/img/apple-touch-icon-144x144-precomposed.png">

    <!-- BASE CSS -->
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="/frontend/css/style.css" rel="stylesheet">
	<link href="/frontend/css/vendors.css" rel="stylesheet">
	
	<!-- ALTERNATIVE COLORS CSS -->
	<link href="#" id="colors" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="/frontend/css/custom.css" rel="stylesheet">

</head>

<body>
    
    <div id="page" class="theia-exception">
        
    <header class="header menu_fixed">
        <div id="preloader"><div data-loader="circle-side"></div></div><!-- /Preload -->
        <div id="logo">
            <a href="index-2.html">
                <img src="/frontend/img/logo.png" width="150" height="36" data-retina="true" alt="" class="logo_normal">
                <img src="/frontend/img/logo_sticky.png" width="150" height="36" data-retina="true" alt="" class="logo_sticky">
            </a>
        </div>
        <ul id="top_menu">
            <li><a href="cart-1.html" class="cart-menu-btn" title="Cart"><strong>4</strong></a></li>
            <li><a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">Sign In</a></li>
            <li><a href="wishlist.html" class="wishlist_bt_top" title="Your wishlist">Your wishlist</a></li>
        </ul>
        <!-- /top_menu -->
        <a href="#menu" class="btn_mobile">
            <div class="hamburger hamburger--spin" id="hamburger">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </a>
        <nav id="menu" class="main-menu">
            <ul>
                <li><span><a href="#0">Home</a></span>
                    <ul>
                        <li><a href="index-2.html">Home version 1</a></li>
                        <li><a href="index-3.html">Home version 2</a></li>
                        <li><a href="index-4.html">Home version 3</a></li>
                        <li><a href="index-5.html">Home version 4</a></li>
                        <li><a href="index-6.html">Home version 5</a></li>
                        <li><a href="index-7.html">With Cookie bar (EU law)</a></li>
                        <li><a href="index-8.html">Home version 7</a></li>
                    </ul>
                </li>
                <li><span><a href="#0">Tours</a></span>
                    <ul>
                        <li><a href="tours-grid-isotope.html">Tours grid isotope</a></li>
                        <li><a href="tours-grid-sidebar.html">Tours grid sidebar</a></li>
                        <li><a href="tours-grid-sidebar-2.html">Tours grid sidebar 2</a></li>
                        <li><a href="tours-grid.html">Tours grid simple</a></li>
                        <li><a href="tours-list-isotope.html">Tours list isotope</a></li>
                        <li><a href="tours-list-sidebar.html">Tours list sidebar</a></li>
                        <li><a href="tours-list-sidebar-2.html">Tours list sidebar 2</a></li>
                        <li><a href="tours-list.html">Tours list simple</a></li>
                        <li><a href="tour-detail.html">Tour detail</a></li>
                    </ul>
                </li>
                <li><span><a href="#0">Hotels</a></span>
                    <ul>
                        <li><a href="hotels-grid-isotope.html">Hotel grid isotope</a></li>
                        <li><a href="hotels-grid-sidebar.html">Hotel grid sidebar</a></li>
                        <li><a href="hotels-grid-sidebar-2.html">Hotel grid sidebar 2</a></li>
                        <li><a href="hotels-grid.html">Hotel grid simple</a></li>
                        <li><a href="hotels-list-isotope.html">Hotel list isotope</a></li>
                        <li><a href="hotels-list-sidebar.html">Hotel list sidebar</a></li>
                        <li><a href="hotels-list-sidebar-2.html">Hotel list sidebar 2</a></li>
                        <li><a href="hotels-list.html">Hotel list simple</a></li>
                        <li><a href="hotel-detail.html">Hotel detail</a></li>
                    </ul>
                </li>
                <li><span><a href="#0">Eat &amp; Drink</a></span>
                    <ul>
                        <li><a href="restaurants-grid-isotope.html">Restaurant grid isotope</a></li>
                        <li><a href="restaurants-grid-sidebar.html">Restaurant grid sidebar</a></li>
                        <li><a href="restaurants-grid-sidebar-2.html">Restaurant grid sidebar 2</a></li>
                        <li><a href="restaurants-grid.html">Restaurant grid simple</a></li>
                        <li><a href="restaurants-list-isotope.html">Restaurant list isotope</a></li>
                        <li><a href="restaurants-list-sidebar.html">Restaurant list sidebar</a></li>
                        <li><a href="restaurants-list-sidebar-2.html">Restaurant list sidebar 2</a></li>
                        <li><a href="restaurants-list.html">Restaurant list simple</a></li>
                        <li><a href="restaurant-detail.html">Restaurant detail</a></li>
                    </ul>
                </li>
                <li><span><a href="#0">Pages</a></span>
                    <ul>
                        <li><a href="about.html">About</a></li>
                        <li><a href="media-gallery.html">Media gallery</a></li>
                        <li><a href="help.html">Help Section</a></li>
                        <li><a href="faq.html">Faq Section</a></li>
                        <li><a href="wishlist.html">Wishlist page</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="blog.html">Blog</a></li>
                    </ul>
                </li>
                <li><span><a href="#0">Extra</a></span>
                    <ul>
                        <li><a href="admin_section/index.html" target="_blank">Admin Dashboard <strong>New!</strong></a></li>
                        <li><a href="menu-options.html">Menu Position Options</a></li>
                        <li><a href="tour-detail-singlemonth-datepicker.html">Single month Datepicker</a></li>
                        <li><a href="404.html">404 Error page</a></li>
                        <li><a href="cart-1.html">Cart page 1</a></li>
                        <li><a href="cart-2.html">Cart page 2</a></li>
                        <li><a href="cart-3.html">Cart page 3</a></li>
                        <li><a href="pricing-tables.html">Responsive pricing tables</a></li>
                        <li><a href="coming_soon/index.html">Coming soon</a></li>
                        <li><a href="invoice.html">Invoice</a></li>
                        <li><a href="icon-pack-1.html">Icon pack 1</a></li>
                        <li><a href="icon-pack-2.html">Icon pack 2</a></li>
                        <li><a href="icon-pack-3.html">Icon pack 3</a></li>
                        <li><a href="icon-pack-4.html">Icon pack 4</a></li>
                        <li><a href="hamburgers.html">Animated Hamburgers</a></li>
                    </ul>
                </li>
                <li><span><a href="https://themeforest.net/item/panagea-travel-and-tours-listings-template/21957086?ref=ansonika" target="_blank">Buy template</a></span></li>
            </ul>
        </nav>

    </header>
    <!-- /header -->

  @yield('content')




    <footer>
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-5 col-md-12 p-r-5">
                    <p><img src="/frontend/img/logo.png" width="150" height="36" data-retina="true" alt=""></p>
                    <p>Mea nibh meis philosophia eu. Duis legimus efficiantur ea sea. Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu. Nihil facilisi indoctum an vix, ut delectus expetendis vis.</p>
                    <div class="follow_us">
                        <ul>
                            <li>Follow us</li>
                            <li><a href="#0"><i class="ti-facebook"></i></a></li>
                            <li><a href="#0"><i class="ti-twitter-alt"></i></a></li>
                            <li><a href="#0"><i class="ti-google"></i></a></li>
                            <li><a href="#0"><i class="ti-pinterest"></i></a></li>
                            <li><a href="#0"><i class="ti-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 ml-lg-auto">
                    <h5>Useful links</h5>
                    <ul class="links">
                        <li><a href="about.html">About</a></li>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="register.html">Register</a></li>
                        <li><a href="blog.html">News &amp; Events</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5>Contact with Us</h5>
                    <ul class="contacts">
                        <li><a href="tel://61280932400"><i class="ti-mobile"></i> + 61 23 8093 3400</a></li>
                        <li><a href="mailto:info@Panagea.com"><i class="ti-email"></i> info@Panagea.com</a></li>
                    </ul>
                    <div id="newsletter">
                    <h6>Newsletter</h6>
                    <div id="message-newsletter"></div>
                    <form method="post" action="http://www.ansonika.com/panagea/assets/newsletter.php" name="newsletter_form" id="newsletter_form">
                        <div class="form-group">
                            <input type="email" name="email_newsletter" id="email_newsletter" class="form-control" placeholder="Your email">
                            <input type="submit" value="Submit" id="submit-newsletter">
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <!--/row-->
            <hr>
            <div class="row">
                <div class="col-lg-6">
                    <ul id="footer-selector">
                        <li>
                            <div class="styled-select" id="lang-selector">
                                <select>
                                    <option value="English" selected>English</option>
                                    <option value="French">French</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="Russian">Russian</option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="styled-select" id="currency-selector">
                                <select>
                                    <option value="US Dollars" selected>US Dollars</option>
                                    <option value="Euro">Euro</option>
                                </select>
                            </div>
                        </li>
                        <li><img src="/frontend/img/cards_all.svg" alt=""></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <ul id="additional_links">
                        <li><a href="#0">Terms and conditions</a></li>
                        <li><a href="#0">Privacy</a></li>
                        <li><span>© 2018 Panagea</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
    </div>
    <!-- page -->
    
    <!-- Sign In Popup -->
    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
        <div class="small-dialog-header">
            <h3>Sign In</h3>
        </div>
        <form>
            <div class="sign-in-wrapper">
                <a href="#0" class="social_bt facebook">Login with Facebook</a>
                <a href="#0" class="social_bt google">Login with Google</a>
                <div class="divider"><span>Or</span></div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" id="email">
                    <i class="icon_mail_alt"></i>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" id="password" value="">
                    <i class="icon_lock_alt"></i>
                </div>
                <div class="clearfix add_bottom_15">
                    <div class="checkboxes float-left">
                        <label class="container_check">Remember me
                          <input type="checkbox">
                          <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
                </div>
                <div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
                <div class="text-center">
                    Don’t have an account? <a href="register.html">Sign up</a>
                </div>
                <div id="forgot_pw">
                    <div class="form-group">
                        <label>Please confirm login email below</label>
                        <input type="email" class="form-control" name="email_forgot" id="email_forgot">
                        <i class="icon_mail_alt"></i>
                    </div>
                    <p>You will receive an email containing a link allowing you to reset your password to a new preferred one.</p>
                    <div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
                </div>
            </div>
        </form>
        <!--form -->
    </div>
    <!-- /Sign In Popup -->
    
    <div id="toTop"></div><!-- Back to top button -->
    
    <!-- COMMON SCRIPTS -->
    <script src="/frontend/js/jquery-2.2.4.min.js"></script>
    <script src="/frontend/js/common_scripts.js"></script>
    <script src="/frontend/js/main.js"></script>
    <script src="/frontend/assets/validate.js"></script>
    
    <!-- Map -->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyB6Vck_vRXDPR8ILH8ZLOeGSEz_n4YR0mU"></script>
    <script src="/frontend/js/map_single_tour.js"></script>
    <script src="/frontend/js/infobox.js"></script>
    
    <!-- DATEPICKER  -->
    <script>
    $(function() {
      $('input[name="dates"]').daterangepicker({
          autoUpdateInput: false,
          opens: 'left',
          locale: {
              cancelLabel: 'Clear'
          }
      });
      $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM-DD-YY') + ' > ' + picker.endDate.format('MM-DD-YY'));
      });
      $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });
    });
    </script>
    
    <!-- INPUT QUANTITY  -->
    <script src="/frontend/js/input_qty.js"></script>
    
    <!-- INSTAGRAM FEED  -->
    <script>
        $(window).on('load', function() {
            "use strict";
            $.instagramFeed({
                'username': 'thelouvremuseum',
                'container': "#instagram-feed",
                'display_profile': false,
                'display_biography': false,
                'display_gallery': true,
                'get_raw_json': false,
                'callback': null,
                'styling': true,
                'items': 12,
                'items_per_row': 6,
                'margin': 1
            });
        });
    </script>
    
    <!-- COLOR SWITCHER  -->
    <script src="/frontend/js/switcher.js"></script>
    <div id="style-switcher">
        <h6>Color Switcher <a href="#"><i class="ti-settings"></i></a></h6>
        <div>
            <ul class="colors" id="color1">
                <li><a href="#" class="default" title="Default"></a></li>
                <li><a href="#" class="aqua" title="Aqua"></a></li>
                <li><a href="#" class="green_switcher" title="Green"></a></li>
                <li><a href="#" class="orange" title="Orange"></a></li>
                <li><a href="#" class="blue" title="Blue"></a></li>
                <li><a href="#" class="beige" title="Beige"></a></li>
                <li><a href="#" class="gray" title="Gray"></a></li>
                <li><a href="#" class="green-2" title="Green"></a></li>
                <li><a href="#" class="navy" title="Navy"></a></li>
                <li><a href="#" class="peach" title="Peach"></a></li>
                <li><a href="#" class="purple" title="Purple"></a></li>
                <li><a href="#" class="red" title="Red"></a></li>
                <li><a href="#" class="violet" title="Violet"></a></li>
            </ul>
        </div>
    </div>
  
</body>