<div class="navbar-default sidebar" role="navigation">

    <div class="sidebar-nav navbar-collapse">

        <ul class="nav" id="side-menu">

          @can('menu-access')
            <li>
                <a href="{{route('exchange-rate.index')}}"> Exchange Rates</a>
            </li>              
            <li>
                <a href="/backend/operator"> Operators</a>
            </li>           
            <li>
                <a href="/backend/package"> Packages</a>
            </li>           
            <li>
                <a href="/backend/check-available-package-list"> Package inquiries</a>
            </li>    
            <li>
                <a href="/backend/quote">Quotes</a>
            </li>       
            <li>
                <a href="/backend/destination"> Destinations</a>
            </li>           
            <li>
                <a href="/backend/users"> Users</a>
            </li>
            <li>
                <a href="/backend/roles"> Roles</a>
            </li>             
            <li>
                <a href="/backend/customer"> Customer</a>
            </li>              
            <li>
                <a href="/backend/faq"> Faq</a>
            </li>            
        @endcan
            <li>
                <a href="/backend-order/order"> Orders</a>
            </li>  
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->