<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'AuthController@login');
Route::get('exchange-rate','Frontend\DepositController@exchange_rate_info');

Route::group(['prefix' => 'auth', 'middleware' => 'jwt.auth'], function () {
    Route::get('user', 'AuthController@user');
    
    Route::post('logout', 'AuthController@logout');
});
Route::get('exchange-rate','Frontend\DepositController@exchange_rate_info');

Route::middleware('jwt.refresh')->get('/token/refresh', 'AuthController@refresh');

Route::group(['prefix' => '{locale}','middleware' => ['is_live_chat','setlocale']], function () {
        // app::setLocale($locale);
	Route::get('welcome-text','Frontend\WebhookController@welcomeText');
	Route::get('package-description','Frontend\WebhookController@packageDescription');
	Route::get('month-list','Frontend\WebhookController@monthList');
	Route::get('quantity-question','Frontend\WebhookController@quantityQuestion');
	Route::get('package-list','Frontend\WebhookController@packageList');
	Route::post('tour-inquiry','Frontend\WebhookController@tourInquiry');
	Route::get('approve-invoice-img','Frontend\WebhookController@approveInvoiceImg');
	Route::get('booking-list','Frontend\WebhookController@bookingList');
	Route::get('faq-list','Frontend\WebhookController@faqList');
	Route::get('faq-more','Frontend\WebhookController@faqMore');
	Route::get('faq-answer','Frontend\WebhookController@faqAnswer');
	Route::get('booking-save-msg','Frontend\DepositController@bookingSaveMsg');
	Route::post('upload-slip','Frontend\DepositController@uploadSlip');
	Route::get('info-payment','Frontend\DepositController@infoPayment');
	Route::resource('webhook','Frontend\WebhookController');
	Route::get('destination-list','Frontend\WebhookController@destinationList');
	Route::get('destination/{id}','Frontend\WebhookController@monthList');
	Route::get('search-package','Frontend\WebhookController@searchPackage');
	Route::get('package/{month}/{destination_id}','Frontend\WebhookController@packageList');
	Route::get('availablepackage','Frontend\WebhookController@availablePackage');
	Route::get('orderconfirm','Frontend\WebhookController@orderConfirm');
	Route::post('check-package','Frontend\WebhookController@checkPackage');
	Route::post('check-quantity','Frontend\WebhookController@checkQuantity');
	Route::post('request-quotation','Frontend\WebhookController@requestQuotation');
	Route::post('rate-packages','Backend\RatePackageController@store');
	Route::post('activate-livechat','Frontend\WebhookController@activateLiveChat');

});
