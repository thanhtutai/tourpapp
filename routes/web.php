<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('email-test', function(){
	$quote = 'yo@gmail.com';


    dispatch(new App\Jobs\SendEmailRequestQuoteJob());


    dd('done');
});
Route::get('email', 'HomeController@sendEmail');

Route::get('send', 'Backend\EmailController@sendingEmail');

Route::get('show-package-img', 'Frontend\WebhookController@showPackageImg');

Route::get('faq', 'Frontend\FaqController@index');

Route::group(['prefix' => 'backend-order','middleware' => ['auth']], function()
{
	Route::resource('order','Backend\OrderController');
});

Route::group(['prefix' => 'frontend'], function()
{
	Route::get('package/{id}','Frontend\PackageController@show');	
});
Route::get('/',function(){
	return view('home');
});
Route::get('lang/{locale}', 'HomeController@lang');


Route::group(['prefix' => 'backend','middleware' => ['auth','permission:menu-access']], function()
{
    Route::get('user', 'AuthController@user');
	Route::post('package-image-upload/{id}','Backend\PackageController@packageImageUpload')->name('package_image.store');
	Route::delete('package-image-upload/{packageid}/{fileid}', [
    'as' => 'destroy', 
    'uses' => 'Backend\PackageController@packageImageDestroy'
	]);	
	Route::delete('approve-invoice-img-destroy/{orderid}/{fileid}', [
    'as' => 'invoice_img_destroy', 
    'uses' => 'Backend\OrderController@approveInvoiceImgDestroy'
	]);
	Route::resource('customer','Backend\CustomerController');
	Route::resource('exchange-rate','Backend\ExchangeRateController');
	Route::resource('operator','Backend\OperatorController');
	Route::resource('quote','Backend\QuoteController');
	Route::resource('faq','Backend\FaqController');
	Route::resource('package','Backend\PackageController');
	Route::resource('destination','Backend\DestinationController');
    Route::resource('roles','Backend\RoleController');
    Route::resource('users','Backend\UserController');
    Route::resource('currency','Backend\CurrencyController');
    Route::put('/orderapprove/{orderid}','Backend\OrderController@orderApprove')->name('order.approve');
	//order from messenger bot
	Route::get('check-available-package/{id}','Backend\PackageController@checkPackage');
	Route::get('approve-invoice-img/{id}','Backend\OrderController@approveInvoiceImg')->name('order.approveInvoiceImg');	
	Route::post('approve-invoice-img-store/{id}','Backend\OrderController@approveInvoiceImgStore')->name('approveInvoiceImg.store');
	Route::get('check-available-package-list','Backend\PackageController@checkPackageList');
	Route::put('check-available-package/{id}','Backend\PackageController@packageInquiryUpdate')->name('check-package.update');
});

// Login
// Authentication Routes...
$this->get('admin/btny', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('admin/btny', 'Auth\LoginController@login');
$this->post('admin/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('admin/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('admin/register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
 $this->post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('admin/password/reset', 'Auth\ResetPasswordController@reset');
